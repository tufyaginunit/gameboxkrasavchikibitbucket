﻿using System;
using System.Reflection;
using UnityEngine;

/// <summary>
/// Расширение функционала компонентов, добавленных на игровые объекты (коллайдеры, меши и т.д.)
/// </summary>
public static class ComponentExtensions
{
    /// <summary>
    /// Скопировать параметры компонента.
    /// </summary>
    /// <typeparam name="T">Тип компонента</typeparam>
    /// <param name="comp">Компонент, в который нужно скопировать параметры</param>
    /// <param name="other">Компонент, из которого копируются параметры</param>
    /// <returns></returns>
    public static T GetCopyOf<T>(this Component comp, T other) where T : Component
    {
        Type type = comp.GetType();
        if (type != other.GetType())
        {
            return null;
        }
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic 
            | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        PropertyInfo[] pinfos = type.GetProperties(flags);
        foreach (var pinfo in pinfos)
        {
            if (pinfo.CanWrite)
            {
                try
                {
                    pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
                }
                catch
                {
                }
            }
        }
        FieldInfo[] finfos = type.GetFields(flags);
        foreach (var finfo in finfos)
        {
            finfo.SetValue(comp, finfo.GetValue(other));
        }
        return comp as T;
    }

    /// <summary>
    /// Добавить на игровой объект компонент, который будет полной копией другого компонента.
    /// Вариант применения: Например, нужно программно добавить на объект коллайдер,
    /// который будет обладать теми же параметрами, что и некоторый другой коллайдер,
    /// но будет триггерным и немного увеличен в размерах.
    /// </summary>
    /// <typeparam name="T">Тип компонента</typeparam>
    /// <param name="go">Игровой объект, на который добавляется компонент</param>
    /// <param name="toAdd">Компонент, из которого копируются параметры</param>
    /// <returns></returns>
    public static T AddComponent<T>(this GameObject go, T toAdd) where T : Component
    {
        return go.AddComponent<T>().GetCopyOf(toAdd) as T;
    }
}
