﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Абстрактный класс, служащий для управления оружием.
/// Добавляется либо на сам объект, совершающий атаку, либо на дочерний объект.
/// </summary>
public abstract class WeaponController : MonoBehaviour, ISaveable
{
    /// <summary>
    /// Значение отклонения угла, при котором можем стрелять по цели,
    /// т.е. насколько мы можем быть не довёрнуты к цели, чтобы иметь возможность стрелять
    /// </summary>
    const float AngleDifferenceEpsilon = 10;

    [SerializeField]
    [Tooltip("Точка, из которой производится атака")]
    protected Transform attackPoint = null;

    [SerializeField]
    [Tooltip("Максимальная дистанция атаки")]
    protected float attackDistance = 10;

    [SerializeField]
    [Tooltip("Время подготовки к совершению атаки")]
    protected float chargeDuration = 1;

    [SerializeField]
    [Tooltip("Время кулдауна после атаки")]
    protected float coolDownDuration = 1;

    /// <summary>
    /// Квадрат максимальной дистанции атаки (для оптимизации)
    /// </summary>
    protected float sqrAttackDistance = 0;

    /// <summary>
    /// Запрошено ли прерывание атаки
    /// </summary>
    protected bool isInterruptionRequested = false;

    /// <summary>
    /// Выполняется ли в данный момент повреждение врага оружием 
    /// (имеет смысл для длительной атаки)
    /// </summary>
    protected bool isHitPerforming = false;

    /// <summary>
    /// Корутина, выполняющая атаку
    /// </summary>
    protected Coroutine attackCoroutine = null;

    /// <summary>
    /// Точка, из которой производится атака
    /// </summary>
    public Transform AttackPoint => attackPoint;

    /// <summary>
    /// Максимальная дистанция атаки
    /// </summary>
    public float AttackDistance => attackDistance;

    /// <summary>
    /// Событие, вызываемое при начале атаки
    /// </summary>
    private Action AttackStarted;

    /// <summary>
    /// Событие, вызываемое при окончании атаки
    /// </summary>
    private Action AttackEnded;

    /// <summary>
    /// Добавить слушателя события начала атаки
    /// </summary>
    /// <param name="listener"></param>
    public void RegisterAttackStartedListener(Action listener)
    {
        AttackStarted += listener;
    }

    /// <summary>
    /// Удалить слушателя события начала атаки
    /// </summary>
    /// <param name="listener"></param>
    public void UnregisterAttackStartedListener(Action listener)
    {
        AttackStarted -= listener;
    }

    /// <summary>
    /// Добавить слушателя события окончания атаки
    /// </summary>
    /// <param name="listener"></param>
    public void RegisterAttackEndedListener(Action listener)
    {
        AttackEnded += listener;
    }

    /// <summary>
    /// Удалить слушателя события окончания атаки
    /// </summary>
    /// <param name="listener"></param>
    public void UnregisterAttackEndedListener(Action listener)
    {
        AttackEnded -= listener;
    }

    /// <summary>
    /// Установить цель атаки
    /// (переопределяется в наследниках)
    /// </summary>
    /// <param name="target"></param>
    public virtual void SetTarget(IDamageable target)
    {
    }

    /// <summary>
    /// Получить цель атаки
    /// (переопределяется в наследниках)
    /// </summary>
    public virtual IDamageable GetTarget()
    {
        return null;
    }

    /// <summary>
    /// Выполнить атаку
    /// </summary>
    public virtual void Attack()
    {
        // Не начинаем новую атаку, если не завершена предыдущая
        if (attackCoroutine == null)
        {
            AttackStarted?.Invoke();
            attackCoroutine = StartCoroutine(PerformAttack());
        }
    }

    /// <summary>
    /// Прервать атаку
    /// </summary>
    public virtual void InterruptAttack()
    {
        isInterruptionRequested = true;
    }

    /// <summary>
    /// Проверить, осуществляется ли в данный момент атака
    /// </summary>
    /// <returns></returns>
    public virtual bool IsAttacking()
    {
        return attackCoroutine != null;
    }

    /// <summary>
    /// Проверить, наносится ли в данный момент урон по цели
    /// </summary>
    /// <returns></returns>
    public virtual bool IsHitPerforming()
    {
        return isHitPerforming;
    }

    /// <summary>
    /// Проверить расстояние до цели 
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public virtual AttackCheckResult CheckTargetAttackDistance(IDamageable target)
    {
        float sqrDistanceToTarget = 
            VectorFunc.GetSqrDistanceXZ(attackPoint.position, target.Transform.position);
        
        if (sqrDistanceToTarget > sqrAttackDistance)
        {
            return AttackCheckResult.TooLongDistance;
        }

        return AttackCheckResult.Ok;
    }

    /// <summary>
    /// Проверить, находится ли цель на линии огня
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public virtual bool IsTargetOnAttackLine(IDamageable target)
    {
        Vector3 vectorToTarget = (target.Transform.position - AttackPoint.position);
        vectorToTarget.y = 0;

        return Vector3.Angle(AttackPoint.forward, vectorToTarget) <= AngleDifferenceEpsilon;
    }

    /// <summary>
    /// Проверить, есть ли препятствия на линии огня
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public virtual bool ThereAreObstaclesOnAttackWay(IDamageable target)
    {
        Vector3 attackVector = (target.Transform.position - attackPoint.position);
        attackVector.y = 0;

        return Physics.Raycast(attackPoint.position, attackVector, attackVector.magnitude, 
            GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore);
    }

    /// <summary>
    /// Установить состояние контроллера оружия по умолчанию
    /// </summary>
    public void SetDefaultState()
    {
        StopAllCoroutines();
        attackCoroutine = null;
        isHitPerforming = false;
        SetTarget(null);
    }

    /// <summary>
    /// Задать состояние объекта. 
    /// Используется для восстановления состояния объекта при возрождении на контрольной точке.
    /// Потенциально метод может быть использован для загрузки из файла,
    /// но сейчас мы используем его только для того, чтобы при начале игры с контрольной точки
    /// восстановить первоначальное состояние объекта
    /// </summary>
    /// <param name="state"></param>
    public void SetState(object state)
    {
        SetDefaultState();
    }

    /// <summary>
    /// Получить состояние объекта.
    /// Потенциально метод может быть использован для сохранения объекта в файл,
    /// сейчас это не требуется, поэтому возвращаем пустую строку в качестве состояния
    /// </summary>
    /// <returns></returns>
    public object GetState()
    {
        return "";
    }

    protected virtual void Awake()
    {
        sqrAttackDistance = attackDistance * attackDistance;
    }

    /// <summary>
    /// Выполнить нанесение урона
    /// </summary>
    protected virtual void Hit()
    {
        isHitPerforming = false;
    }

    /// <summary>
    /// Корутина, выполняющая атаку
    /// </summary>
    /// <returns></returns>
    private IEnumerator PerformAttack()
    {
        AttackStarted?.Invoke();
        isInterruptionRequested = false;

        // Выжидаем время подготовки к атаке.
        float hitTime = Time.time + chargeDuration;
        while (Time.time < hitTime)
        {
            yield return null;

            if (isInterruptionRequested)
            {
                AttackEnded?.Invoke();
                attackCoroutine = null;
                yield break;
            }
        }

        // Наносим удар (выполняется выстрел или наносится длительный урон лучом)
        isHitPerforming = true;
        Hit();

        // Ожидаем окончания удара
        while (isHitPerforming)
        {
            yield return null;
        }

        AttackEnded?.Invoke();

        // Ожидаем, пока пройдет кулдаун. Только тогда можно будет атаковать снова.
        if (coolDownDuration > 0)
        {
            yield return new WaitForSeconds(coolDownDuration);
        }

        attackCoroutine = null;
    }

}
