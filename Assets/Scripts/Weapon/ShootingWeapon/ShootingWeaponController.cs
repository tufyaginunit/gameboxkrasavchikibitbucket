using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ���������� ������, ����������� ������
/// </summary>
public class ShootingWeaponController : WeaponController
{
    [SerializeField]
    [Tooltip("������, ���������� �� �������� ����")]
    private Shooting shooting = null;

    /// <summary>
    /// ����, �� ������� ��������
    /// </summary>
    private IDamageable target = null;

    /// <summary>
    /// ���������� ����
    /// </summary>
    /// <param name="target"></param>
    public override void SetTarget(IDamageable target)
    {
        this.target = target;
    }

    /// <summary>
    /// �������� ����
    /// </summary>
    /// <returns></returns>
    public override IDamageable GetTarget()
    {
        return target;
    }

    /// <summary>
    /// ���� �� ����������� �� ����� �����.
    /// ����������� �����, �.�. ����� ��������� ������ ����
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public override bool ThereAreObstaclesOnAttackWay(IDamageable target)
    {
        // ���������� SphereCast, �.�. ����� ��������� ������ ����, ��������� ����������� �� ����.
        // �� ����� ����� ��������� �����. ���� ����� ��������� ����������� ������ ��
        // ���������� �� ����� ����� �� ����, �� ��� ������� ������� ���� ����� SphereCast ����� �����
        // ����� �� �����, ���� �� ����� �������� �����. ������� ����������� ����� ������������ ������� �� 
        // ������� ����.
        // ����� ������ ����� ��������� ��� � �����, ���� ��������� ����� ����� ����� ���, � SphereCast �����
        // �������� ��������, ��� ����������� ���. ������� ������ �������������� SphereCast ������, �����
        // ����������, �������� �� ���� � �����������

        float bulletSize = shooting.BulletBounds.extents.x;

        Vector3 topPoint = attackPoint.position;
        topPoint.y = VectorFunc.MaxRaycastDistance;

        if (Physics.SphereCast(topPoint, bulletSize + 0.01f, Vector3.down, out _, 
            VectorFunc.MaxRaycastDistance - attackPoint.position.y, 
            GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore))
        {
            return true;
        }

        Vector3 attackVector = target.Transform.position - attackPoint.position;
        attackVector.y = 0;
        float attackVectorLength = attackVector.magnitude - bulletSize * 2;        

        return Physics.SphereCast(attackPoint.position, bulletSize, attackVector, out _,
            attackVectorLength, GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore);
    }

    /// <summary>
    /// ��������� �������
    /// </summary>
    protected override void Hit()
    {
        Vector3 attackDirection;
        if (target != null)
        {
            attackDirection = (target.Transform.position - attackPoint.position).normalized;
            attackDirection.y = 0;
        }
        else
        {
            attackDirection = attackPoint.forward;
        }

        Transform targetTransform = target.Transform;
        Quaternion rotation = Quaternion.LookRotation(attackDirection);

        shooting.Shoot(attackPoint.position,
                        rotation,
                        Vector3.zero,
                        targetTransform,
                        1);

        isHitPerforming = false;
    }

}
