using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPoolContainerSingleton : MonoBehaviour
{
    public static BulletPoolContainerSingleton Instance { get; private set; }

    private BulletPool pool;

    [SerializeField]
    private Bullet bulletPrefab;

    public BulletPool Pool => pool; 


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        pool = new BulletPool(bulletPrefab, 5, Instance.transform);
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }
}
