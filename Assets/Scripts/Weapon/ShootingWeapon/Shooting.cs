using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// �������� ������ ������: ������� ���� ���� ��������� ����������� �� ��������� ������� �
/// � ������ Shoot ���������� ��������� � ������� ��������� ����.
/// </summary>
public class Shooting : MonoBehaviour
{  
    [SerializeField] private Bullet bulletPrefab; //����� ��������� ��������� ��� ���� �� ����� ����������
    [SerializeField] private bool isInheritingSourceVelocity = false;

    public Bounds BulletBounds { get; private set; }
    public float DefaultDamageValue => bulletPrefab.DefaultDamageValue;

    private void Awake()
    {
        // �������� ������ ���� �� ������� ������. ������� ������� ��������� ������, ����� ������, � ����������
        Bullet bullet = Instantiate(bulletPrefab, Vector3.zero, Quaternion.identity);
        Collider bulletCollider = bullet.GetComponent<Collider>();
        BulletBounds = bulletCollider.bounds;
        Destroy(bullet.gameObject);
    }

    public void Shoot(Vector3 startDirection, Quaternion startRotation, Vector3 startVelocity, 
        Transform targetBulletPoint, float damageMultiplier)
    {
        if (!isInheritingSourceVelocity) startVelocity = Vector3.zero;

        BulletPoolContainerSingleton.Instance.Pool.EmitBullet(gameObject, startDirection, startRotation, 
            startVelocity, targetBulletPoint, damageMultiplier);
    }
}
