﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Атакующий луч, повреждающий владельца
/// </summary>
public class SelfDamageAttackBeam : AttackBeam
{
    [SerializeField]
    [Tooltip("Повреждаемый объект, которому будет наноситься урон " +
        "одновременно с нанесением урона врагу ")]
    private DamageableObject healthObject;

    [SerializeField]
    [Tooltip("Значение множителя урона")]
    private float damageMultiplier = 1.6f;

    [SerializeField]
    [Tooltip("Значение HP, при котором достигается максимальное значение урона, т.е. " +
        "при более низком значении HP урон больше не растёт")]
    private int minHealthForDamageIncrease = 20;

    /// <summary>
    /// Словарь, содержащий пары <враг, потраченное_здоровье>. 
    /// Служит для восстановления ровно такого количество HP игрока при убийстве врага, которое было 
    /// на него потрачено
    /// </summary>
    private readonly Dictionary<IDamageable, int> spentHealthDictionary = new Dictionary<IDamageable, int>();

    /// <summary>
    /// Начать атаку
    /// </summary>
    public override void StartAttack()
    {
        base.StartAttack();
        healthObject.SetEnabledRegeneration(false);
    }

    /// <summary>
    /// Завершить атаку
    /// </summary>
    public override void StopAttack()
    {
        base.StopAttack();
        healthObject.SetEnabledRegeneration(true);
    }

    protected override void Awake()
    {
        base.Awake();
        healthObject.RegisterRevivedListener(OnRevived);
    }

    /// <summary>
    /// Проверить, можно ли продолжать атаку
    /// </summary>
    /// <returns></returns>
    protected override bool AreHitConditionsSatisfied()
    {
        return base.AreHitConditionsSatisfied() && healthObject.Health > 1;
    }

    /// <summary>
    /// Выполнить нанесение урона
    /// </summary>
    /// <param name="damageValue">Базовое значение урона</param>
    protected override void CauseDamage(float damageValue)
    {       
        // Наносим урон врагу и игроку.
        // Если враг умирает, игрок восстанавливает столько здоровья, сколько он потратил на врага.

        target.ReceiveDamage((int)CalculateDamage(damageValue));

        int spentHealth = (int)damageValue;
        //атакующий игрок
        healthObject.ReceiveDamage(spentHealth);

        int spentHealthSum = spentHealth;
        if (spentHealthDictionary.TryGetValue(target, out int previousSpentHealthSum))
        {
            spentHealthSum += previousSpentHealthSum;
        }

        spentHealthDictionary[target] = spentHealthSum;

        if (!target.IsAlive())
        {
            healthObject.AddHealth(spentHealthDictionary[target]);
            spentHealthDictionary.Remove(target);

            healthObject.HealEffect();
        }
    }

    protected override IEnumerator NoTargetHitting()
    {
        float accumulatedDamageValue = 0;
        float hitDuration = 0;

        Coroutine raycastCoroutine = StartCoroutine(RaycastRefreshing());
        float endTime = Time.time + noTargetHittingTime;
        while (Time.time < endTime)
        {
            // Если при выстреле вхолостую удалось найти врага, 
            // то выйдем из цикла и запустим корутину стрельбы по врагу.            
            if (target != null)
            {                
                break;
            }
            RefreshBeamPosition();

            // Поскольку здоровье измеряется в целых единицах, не будем наносить урон каждый кадр
            // т.к. при значении меньше 1, будет наносится 0 урона.
            // Будем копить урон каждый кадр и наносить только тогда, когда значение будет больше 1.
            int accumulatedDamageValueInt = (int)accumulatedDamageValue;
            if (accumulatedDamageValueInt > 0)
            {
                if (healthObject.Health > 0) healthObject.ReceiveDamage((int)accumulatedDamageValue);
                accumulatedDamageValue = 0;
            }
            yield return new WaitForFixedUpdate();

            hitDuration += Time.fixedDeltaTime;
            accumulatedDamageValue += damagePerSecond * Time.fixedDeltaTime;
        }
        StopCoroutine(raycastCoroutine);

        if (target != null)
        {
            StartCoroutine(Hitting());
        }
        else
        {
            StopAttack();
        }
    }

    private void OnDestroy()
    {
        healthObject.UnregisterRevivedListener(OnRevived);
    }

    /// <summary>
    /// Рассчитать урон с учётом базового значения урона, текущего здоровья игрока
    /// и множителя
    /// </summary>
    /// <param name="damageValue">Базовое значение урона</param>
    /// <returns></returns>
    private float CalculateDamage(float damageValue)
    {
        int healthMultiplier = healthObject.Health >= minHealthForDamageIncrease ? 
            healthObject.Health : minHealthForDamageIncrease;
        return damageValue * 100 / healthMultiplier
            * (damageMultiplier - (100 - healthMultiplier) / 200);
    }

    /// <summary>
    /// Обработчик события возрождения с контрольной точки.
    /// Служит для того, чтобы при перезапуске уровня очистить словарь 
    /// потраченного на врагов значения HP
    /// </summary>
    private void OnRevived()
    {
        spentHealthDictionary.Clear();
    }
}
