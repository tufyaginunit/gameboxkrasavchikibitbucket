﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Абстрактный класс оружия, представляющего собой атакующий луч
/// Используется в BeamWeaponController, который управляет процессом атаки этим оружием
/// </summary>
public abstract class AttackBeam : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Значение урона в секунду")]
    protected float damagePerSecond = 20;

    [SerializeField]
    [Tooltip("Время атаки вхолостую (когда нет цели)")]
    protected float noTargetHittingTime = 0.4f;

    [SerializeField]
    [Tooltip("Отрисовщик, отвечающий за визуальное отображение атакующего луча")]
    protected AttackBeamRenderer beamRenderer = null;

    [SerializeField]
    [Tooltip("Визуальная точка атаки, в которой будет начинаться луч " +
        "(может не совпадать с фактической точкой атаки)")]
    protected Transform visualAttackPoint = null;

    [SerializeField]
    [Tooltip("Препятствие, которое создаёт луч для врагов")]
    protected BeamObstacle beamObstacle = null;

    /// <summary>
    /// Для получения максимальной длины луча
    /// </summary>
    public float MaxDistance => maxDistance;



    /// <summary>
    /// Текущая цель, по которой наносится урон
    /// </summary>
    protected IDamageable target = null;

    /// <summary>
    /// Максимальная длина луча
    /// </summary>
    protected float maxDistance = 0;

    /// <summary>
    /// Фактическая точка атаки
    /// </summary>
    protected Transform attackPoint = null;

    /// <summary>
    /// Параметры последнего рэйкаста
    /// </summary>
    protected RaycastHit currentRaycastHit;
    
    /// <summary>
    /// Выполняется ли атака по цели
    /// </summary>
    protected bool isAttacking = false;

    /// <summary>
    /// Инициализировать атакующий луч.
    /// Необходимо вызывать перед StartAttack()
    /// </summary>
    /// <param name="attackPoint">Фактическая точка атаки</param>
    /// <param name="maxDistance">Максимальная дистанция атаки</param>
    public virtual void Initialize(Transform attackPoint, float maxDistance)
    {
        this.attackPoint = attackPoint;
        this.maxDistance = maxDistance;
    }

    /// <summary>
    /// Установить цель для атаки
    /// </summary>
    /// <param name="target"></param>
    public virtual void SetTarget(IDamageable target)
    {
        this.target = target;
    }

    /// <summary>
    /// Получить текущую цель атаки
    /// </summary>
    /// <returns></returns>
    public virtual IDamageable GetTarget()
    {
        return target;
    }

    /// <summary>
    /// Проверить, выполняется ли атака
    /// </summary>
    /// <returns></returns>
    public virtual bool IsAttacking()
    {
        return isAttacking;
    }

    /// <summary>
    /// Начать атаку
    /// </summary>
    public virtual void StartAttack()
    {
        SetActiveBeam(true);
        isAttacking = true;
        if (target != null)
        {
            StartCoroutine(Hitting());            
        }
        else
        {
            StartCoroutine(NoTargetHitting());
        }
    }

    /// <summary>
    /// Остановить атаку
    /// </summary>
    public virtual void StopAttack()
    {
        StopAllCoroutines();
        isAttacking = false;
        SetActiveBeam(false);
        target = null;
    }

    protected virtual void Awake()
    {
        SetActiveBeam(false);
        beamRenderer.Initialize(this); //кладем в отрисовщика сам луч, теперь отрисовщик знает о луче
    }

    /// <summary>
    /// Проверить, можно ли продолжать атаку
    /// </summary>
    /// <returns></returns>
    protected virtual bool AreHitConditionsSatisfied()
    {
        return target != null && target.IsAlive();
    }

    /// <summary>
    /// Выполнить нанесение урона
    /// </summary>
    /// <param name="damageValue">Базовое значение урона</param>
    protected virtual void CauseDamage(float damageValue)
    {
        target.ReceiveDamage((int)(damageValue));
    }

    /// <summary>
    /// Корутина, выполяющая длительное повреждение врага лучом
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator Hitting()
    {
        float accumulatedDamageValue = 0;
        float hitDuration = 0;

        Coroutine raycastCoroutine = StartCoroutine(RaycastRefreshing());

        while (AreHitConditionsSatisfied())
        {
            RefreshBeamPosition();

            // Поскольку здоровье измеряется в целых единицах, не будем наносить урон каждый кадр
            // т.к. при значении меньше 1, будет наносится 0 урона.
            // Будем копить урон каждый кадр и наносить только тогда, когда значение будет больше 1.
            int accumulatedDamageValueInt = (int)accumulatedDamageValue;
            if (accumulatedDamageValueInt > 0)
            {
                CauseDamage(accumulatedDamageValue);
                accumulatedDamageValue = 0;
            }
            
            yield return new WaitForFixedUpdate();
            hitDuration += Time.fixedDeltaTime;
            accumulatedDamageValue += damagePerSecond * Time.fixedDeltaTime;
        }
        StopCoroutine(raycastCoroutine);

        StopAttack();
    }

    /// <summary>
    /// Корутина, выполяющая выстрел лучом вхолостую
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator NoTargetHitting()
    {
        Coroutine raycastCoroutine = StartCoroutine(RaycastRefreshing());
        float endTime = Time.time + noTargetHittingTime;
        while (Time.time < endTime)
        {
            // Если при выстреле вхолостую удалось найти врага, 
            // то выйдем из цикла и запустим корутину стрельбы по врагу.            
            if (target != null)
            {
                break;
            }

            RefreshBeamPosition();

            yield return new WaitForFixedUpdate();
        }
        StopCoroutine(raycastCoroutine);

        if (target != null)
        {
            StartCoroutine(Hitting());
        }
        else
        {
            StopAttack();
        }
    }

    /// <summary>
    /// Активировать/деактивировать луч
    /// </summary>
    /// <param name="active"></param>
    protected void SetActiveBeam(bool active)
    {
        beamRenderer.SetActive(active);
        beamObstacle.SetActive(active);
    }

    /// <summary>
    /// Обновить позицию луча
    /// </summary>
    protected void RefreshBeamPosition()
    {

        Vector3 startPoint = visualAttackPoint.position;
        Vector3 endPoint = GetBeamEndPoint();

        beamRenderer.SetPosition(startPoint, endPoint);
        beamObstacle.SetPosition(startPoint, endPoint);
    }

    /// <summary>
    /// Обновить параметры рэйкаста луча, пытаясь обнаружить цель
    /// </summary>
    protected virtual void RefreshTargetRaycast()
    {
        currentRaycastHit = GetAttackRaycast();
        target = null;
        if (currentRaycastHit.collider != null)
        {
            target = currentRaycastHit.collider.GetComponent<IDamageable>();
        }
    }

    /// <summary>
    /// Корутина, выполняющая обновление рэйкаста луча
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator RaycastRefreshing()
    {       
        while (true)
        {
            RefreshTargetRaycast();
            yield return new WaitForFixedUpdate();
        }
    }

    /// <summary>
    /// Получить конечную точку луча
    /// </summary>
    /// <returns></returns>
    protected virtual Vector3 GetBeamEndPoint()
    {
        if (currentRaycastHit.collider == null)
        {
            return visualAttackPoint.position + attackPoint.forward * maxDistance;
        }
        else
        {
            return currentRaycastHit.point;
        }
    }

    /// <summary>
    /// Получить рэйкаст атакующего луча в текущем направлении атаки
    /// </summary>
    /// <returns></returns>
    protected virtual RaycastHit GetAttackRaycast()
    {
        Vector3 direction = attackPoint.forward;
        float distance = maxDistance;

        // Если уже ведётся огонь по цели, доворачиваем луч к ней
        if (target != null)
        {
            direction = target.Transform.position - attackPoint.position;
            direction.y = 0;
        }

        // Если на пути луча есть препятствие, то ограничиваем максимальную длину луча расстоянием до
        // препятствия
        if (Physics.Raycast(attackPoint.position, direction, out RaycastHit obstacleHitInfo, distance,
            GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore))
        {
            distance = obstacleHitInfo.distance;
        }

        // Пытаемся продолжать атаку по текущей цели, обновляем расстояние до неё
        Ray ray = new Ray(attackPoint.position, direction);
        if (target != null &&
            currentRaycastHit.collider != null && 
            currentRaycastHit.collider.transform == target.Transform &&
            currentRaycastHit.collider.Raycast(ray, out RaycastHit hitInfo, distance))
        {
            return hitInfo;
        }

        // Цели нет (или не удалось продолжить атаку по ней). Пытаемся найти цель
        if (Physics.Raycast(attackPoint.position, direction, out hitInfo, distance,
            GameManager.Instance.EnemyLayers, QueryTriggerInteraction.Ignore))
        {
            return hitInfo;
        }

        // Цель не нашли, возвращаем информацию о найденном препятствии на пути луча
        return obstacleHitInfo;
    }
}
