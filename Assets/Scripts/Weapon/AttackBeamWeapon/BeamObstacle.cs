using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// �����������, ������� ������ ��� ��� ������.
/// ������� ������� ����������� ����������, ������� ����� ������������� �����.
/// </summary>
[RequireComponent(typeof(CapsuleCollider))]
public class BeamObstacle : MonoBehaviour
{
    [SerializeField]
    [Tooltip("������� ��� �������� ��������� ������������ ������")]
    protected bool isEnabled = true;

    /// <summary>
    /// ���������, ������������� ������
    /// </summary>
    private CapsuleCollider beamCollider = null;

    private void Awake()
    {
        beamCollider = GetComponent<CapsuleCollider>();
        gameObject.SetActive(false);
    }
    /// <summary>
    /// ��������/��������� ����������� ����
    /// </summary>
    /// <param name="active"></param>
    public void SetActive(bool active)
    {
        if (isEnabled)
        {
            gameObject.SetActive(active);
        }       
    }

    /// <summary>
    /// ���������� ��������� � �������� ����� ����
    /// </summary>
    /// <param name="startPoint"></param>
    /// <param name="endPoint"></param>
    public void SetPosition(Vector3 startPoint, Vector3 endPoint)
    {
        startPoint.y = endPoint.y;
        
        float dist = Vector3.Distance(startPoint, endPoint);

        // ���� ���� ������� ������, ��������� ���������, ����� �������� ��� �������
        beamCollider.enabled = dist > 2 * beamCollider.radius;
        if (!beamCollider.enabled)
        {
            return;
        }

        Vector3 direction = (endPoint - startPoint) / dist;

        // ������ ����� ���������� ����� � ���������, ����� ����� ���������� �� �������� �� �����,
        // ����� �� �� ����� ����� ����������� �� ����.
        startPoint -= 2 * beamCollider.radius * direction;
        transform.position = startPoint + direction * dist / 2;
        transform.forward = direction;

        beamCollider.height = dist;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent(out AIMovingObject movingObject))
        {
            Vector3 objPosition = movingObject.Position;
            objPosition.y = transform.position.y;

            // ����������� ����� �� ���� ����, ���� �����
            Vector3 vectorToObject = objPosition - transform.position;
            vectorToObject.y = 0;
            Vector3 pushDirection = VectorFunc.GetRotatedUnitVector(transform.forward, 90);
            
            if (Vector3.Dot(vectorToObject, pushDirection) < 0)
            {
                pushDirection = -pushDirection;
            }

            float checkDistance = beamCollider.radius * 2;
            if (other.GetType() == typeof(CapsuleCollider))
            {
                checkDistance = (other as CapsuleCollider).radius + beamCollider.radius;
            }

            Vector3 pointOnBeam = VectorFunc.GetProjectionOnDirection(vectorToObject, transform.forward);

            Vector3 pushVector = pushDirection * (checkDistance - Vector3.Distance(pointOnBeam, vectorToObject));
            // ���� �������� ����� � ���������� ����� ����������, � ����� ������ �������� ���
            // � ��������������� �������
            if (!movingObject.CalculatePathToPoint(objPosition + pushVector, out _))
            {
                pushVector = -pushVector;
            }

            movingObject.JumpToPoint(objPosition + pushVector);
        }
    }
}
