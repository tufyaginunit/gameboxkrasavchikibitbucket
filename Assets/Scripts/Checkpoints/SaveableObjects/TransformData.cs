﻿using UnityEngine;
using System;
/// <summary>
/// Сериализуемые данные трансформа объекта
/// </summary>
[Serializable]
public struct TransformData
{
    public Vector3 position;
    public Quaternion rotation;
}



