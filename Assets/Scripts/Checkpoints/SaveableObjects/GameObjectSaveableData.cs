﻿using UnityEngine;

/// <summary>
/// Сохраняемые компоненты игрового объекта 
/// </summary>
public struct GameObjectSaveableData
{
    /// <summary>
    /// Сохраняемы игровой объект
    /// </summary>
    public GameObject gameObject;

    /// <summary>
    /// Компоненты, способные себя сохранять и загружать, находящиеся на данном gameobject
    /// и его дочерних gameobject
    /// </summary>
    public SaveableEntity[] saveables;

    public GameObjectSaveableData(GameObject gameObject) : this()
    {
        this.gameObject = gameObject;
        saveables = gameObject.GetComponentsInChildren<SaveableEntity>();
        SaveData();
    }

    /// <summary>
    /// Сохранить состояние объекта
    /// </summary>
    public void SaveData()
    {
        foreach (var item in saveables)
        {
            item.Save();
        }
    }

    /// <summary>
    /// Загрузить состояние объекта
    /// </summary>
    public void LoadData()
    {
        foreach (var item in saveables)
        {
            item.Load();
        }
    }
}
