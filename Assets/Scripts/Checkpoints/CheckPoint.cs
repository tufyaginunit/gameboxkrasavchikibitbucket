using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

/// <summary>
/// ����������� �����, �� ������� ������������ �������� ����� ������.
/// ����������� ��� ��������� �������� ������ � ������ ����������� ����������� �����
/// ������ ������ ������, ������� ������ ������������ ��� ��������� ��� ����������� ������,
/// ����� ����������� ������
/// </summary>
public class CheckPoint : MonoBehaviour
{   
    [SerializeField]
    [Tooltip("�����, �� ������� ���������� �����")]
    private Transform revivePoint = null;

    /// <summary>
    /// �����, ������� ����� �����, ����� ������������ ����������� �����
    /// </summary>
    private readonly List<IDamageable> enemies = new List<IDamageable>();

    /// <summary>
    /// ������ ������ ��� �������������� ��� ����������� � ����������� �����
    /// </summary>
    private readonly List<GameObjectSaveableData> enemiesSaveableData = new List<GameObjectSaveableData>();

    /// <summary>
    /// ������ ������ ��� �������������� ��� ����������� � ����������� �����
    /// </summary>
    public List<GameObjectSaveableData> EnemySaveableDataList => enemiesSaveableData;

    public Transform RevivePoint => revivePoint;

    public List<IDamageable> Enemies => enemies;

    /// <summary>
    /// �������, ���������� � ������ ������� ������ ������������ ����������� �����
    /// </summary>
    private event Action<CheckPoint> ActivationRequested;

    /// <summary>
    /// �������� ��������� ������� ������� ��������� ����������� �����
    /// </summary>
    /// <param name="listener"></param>
    public void RegisterActivationRequestedListener(Action<CheckPoint> listener)
    {
        ActivationRequested += listener;
    }

    /// <summary>
    /// ������� ��������� ������� ������� ��������� ����������� �����
    /// </summary>
    /// <param name="listener"></param>
    public void UnregisterActivationRequestedListener(Action<CheckPoint> listener)
    {
        ActivationRequested -= listener;
    }

    /// <summary>
    /// ��������� ��������� ����������� �����.
    /// �������� ���� �� �������� �����������, ����� ����� ������������ � ���
    /// </summary>
    public void RequestActivation()
    {
        ActivationRequested?.Invoke(this);
    }

    /// <summary>
    /// �������� ����� � ������ �������� �������������� ����������� �����
    /// </summary>
    /// <param name="enemy"></param>
    public void AddEnemy(IDamageable enemy)
    {
        enemiesSaveableData.Add(new GameObjectSaveableData(enemy.Transform.gameObject));
        enemies.Add(enemy);
    }

    /// <summary>
    /// �������� ���������� ������, ������� �������� ����� ��� ��������� ����������� �����
    /// </summary>
    /// <returns></returns>
    public int GetAliveEnemiesCount()
    {
        int aliveEnemiesCount = 0;
        foreach (var enemy in enemies)
        {
            if (enemy.IsAlive())
            {
                aliveEnemiesCount++;
            }
        }
        return aliveEnemiesCount;
    }

}
