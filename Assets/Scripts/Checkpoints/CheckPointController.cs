using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ���������� ����������� �����.
/// ������ ����������� �����, �� ������� ����� ����������� ��������.
/// ��������������� ��������� ���� ��� ����������� �� ������������ �����
/// </summary>
public class CheckPointController : MonoBehaviour
{
    [SerializeField]
    [Tooltip("����, �� ������� ������������� ���������� ��� ������ ��� ����������� �����")]
    private LayerMask checkPointAreaLayers;

    [SerializeField]
    [Tooltip("�������� ���������� �������� �������� ����������� �����")]
    private bool debugDraw = true;

    /// <summary>
    /// ������ ����������� �����
    /// </summary>
    private List<CheckPoint> checkPoints = null;

    /// <summary>
    /// ������ ��� �������������� ������
    /// </summary>
    private GameObjectSaveableData playerSaveableObject;

    /// <summary>
    /// ������ ������� �������������� ����������� �����
    /// </summary>
    private int currentActiveCheckpointIndex = -1;

    /// <summary>
    /// ���������� ���� �������� ����������� �����
    /// </summary>
    private MeshRenderer[] debugMeshRenderers = null;

    /// <summary>
    /// ������� ��������� ���������� ���������
    /// </summary>
    private bool currentDebugDrawState = false;

    private void Awake()
    {
        // ������������� ������� ��������� � �������� ��������.
        checkPoints = new List<CheckPoint>();
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).TryGetComponent(out CheckPoint checkPoint))
            {
                checkPoints.Add(checkPoint);
            }
        }

        debugMeshRenderers = GetComponentsInChildren<MeshRenderer>();
        SetDebugDrawState(debugDraw);
    }

    private void Start()
    {
        playerSaveableObject = new GameObjectSaveableData(GameManager.Instance.Player.gameObject);

        foreach (var item in checkPoints)
        {
            item.RegisterActivationRequestedListener(OnCheckPointActivationRequested);
        }

        SetEnemiesToCheckpoints();          
    }

    private void OnDestroy()
    {
        foreach (var item in checkPoints)
        {
            item.UnregisterActivationRequestedListener(OnCheckPointActivationRequested);
        }
    }

    private void Update()
    {
        if (debugDraw != currentDebugDrawState)
        {
            SetDebugDrawState(debugDraw);
        }
    }

    /// <summary>
    /// ��������/��������� ���������� ���������
    /// </summary>
    /// <param name="enabled">��������� ��������/���������</param>
    private void SetDebugDrawState(bool enabled)
    {
        foreach (var meshRenderer in debugMeshRenderers)
        {
            meshRenderer.enabled = enabled;
        }
        currentDebugDrawState = enabled;
    }

    /// <summary>
    /// �������� ������ ����������� �����
    /// </summary>
    /// <param name="checkPoint"></param>
    /// <returns></returns>
    private int GetCheckPointIndex(CheckPoint checkPoint)
    {
        return checkPoints.IndexOf(checkPoint);
    }

    /// <summary>
    /// ���������, ������������ �� ����������� �����
    /// </summary>
    /// <param name="checkPointIndex"></param>
    /// <returns></returns>
    private bool IsCheckPointActivated(int checkPointIndex)
    {
        return checkPointIndex <= currentActiveCheckpointIndex;
    }

    /// <summary>
    /// ���������, �������� �� ����������� ����� ��������� ������, ������� ����� ������������
    /// </summary>
    /// <param name="checkPointIndex"></param>
    /// <returns></returns>
    private bool IsNextCheckPointToActivate(int checkPointIndex)
    {
        return checkPointIndex == currentActiveCheckpointIndex + 1;
    }

    /// <summary>
    /// ���������, ����� �� ����� ����������� � �����-���� ����������� �����.
    /// ���������� true, ���� ����� ����������� ���� �� ���� ����������� �����,
    /// false - � ��������� ������
    /// </summary>
    /// <returns></returns>
    public bool CanReviveFromCheckPoint()
    {
        return currentActiveCheckpointIndex != -1;
    }

    /// <summary>
    /// ��������� ��������� ���� �� ��������� �������������� ����������� �����
    /// </summary>
    public void LoadLastCheckPointState()
    {
        // ��������������� ������.
        // ����� ��� ����� ������, ������� ����������� ��� �� �������������� ����������� ������,
        // �� ����� ������������, �.�. �� ������ ��������� ��������� ����������� ����� ��� ����.
        for (int i = currentActiveCheckpointIndex + 1; i < checkPoints.Count; i++)
        {
            foreach (var enemy in checkPoints[i].EnemySaveableDataList)
            {
                enemy.gameObject.SetActive(true);
                enemy.LoadData();
            }
        }

        // ��������������� ������. �� ��������� �� ����� ������������ � ������ ������.
        playerSaveableObject.LoadData();
        
        // ���� ����� ����������� ���� �� ���� ����������� �����, ����� ����� �������� ����. 
        if (CanReviveFromCheckPoint())
        {
            Transform playerTransform = playerSaveableObject.gameObject.transform;
            Vector3 revivePosition = 
                checkPoints[currentActiveCheckpointIndex].RevivePoint.position;
            revivePosition.y = playerTransform.position.y;
            playerTransform.position = revivePosition;
        }
    }

    /// <summary>
    /// ��������� ������� ������� ��������� ����������� ����� ������� 
    /// </summary>
    /// <param name="checkPoint">����������� �����, ������� ����� �������� ������������</param>
    private void OnCheckPointActivationRequested(CheckPoint checkPoint)
    {
        int aliveEnemiesCount = checkPoint.GetAliveEnemiesCount();
        if (aliveEnemiesCount > 0)
        {
            Debug.Log($"{checkPoint.name}: {aliveEnemiesCount} enemies left to defeat");
            return;
        }

        int checkPointIndex = GetCheckPointIndex(checkPoint);

        if (IsCheckPointActivated(checkPointIndex))
        {
            Debug.Log($"{checkPoint.name}: Check point is already activated");
            return;
        }

        if (!IsNextCheckPointToActivate(checkPointIndex))
        {
            Debug.Log($"{checkPoint.name}: You have to activate previous checkpoint");
            return;
        }

        currentActiveCheckpointIndex = checkPointIndex;

        Debug.Log($"{checkPoint.name}: Checkpoint activated");
    }

    /// <summary>
    /// ��������� ������ ��������������� ����������� ������, � ���� ������� ��� ���������
    /// </summary>
    private void SetEnemiesToCheckpoints()
    {
        Enemy[] enemies = FindObjectsOfType<Enemy>();
        foreach (var enemy in enemies)
        {
            Vector3 topPoint = enemy.Transform.position;
            topPoint.y = VectorFunc.MaxRaycastDistance;
            if (!Physics.Raycast(topPoint, Vector3.down, out RaycastHit hitInfo,
                VectorFunc.MaxRaycastDistance * 2, checkPointAreaLayers))
            {
                continue;
            }

            if (!hitInfo.transform.TryGetComponent(out CheckPointAreaCollider areaCollider))
            {
                continue;
            }

            areaCollider.Owner.AddEnemy(enemy);
        }
    }
}
