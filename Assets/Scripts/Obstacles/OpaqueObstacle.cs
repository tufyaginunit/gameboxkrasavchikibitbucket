﻿using UnityEngine;

/// <summary>
/// Всегда видимое препятствие. 
/// </summary>
public class OpaqueObstacle : MonoBehaviour, IObstacle
{
    /// <summary>
    /// Коллайдеры препятствия
    /// </summary>
    private Collider[] colliders = null;

    /// <summary>
    /// Трансформ препятствия
    /// </summary>
    public Transform Transform => transform;

    /// <summary>
    /// Тип препятствия
    /// </summary>
    public ObstacleType Type => ObstacleType.AlwaysOpaque;

    /// <summary>
    /// Можно ли в данный момент пройти через препятствие
    /// </summary>
    /// <returns></returns>
    public bool IsPassable()
    {
        return false;
    }

    /// <summary>
    /// Является в данный момент препятствие полностью непрозрачным
    /// </summary>
    /// <returns></returns>
    public bool IsOpaque()
    {
        return true;
    }

    /// <summary>
    /// Получить коллайдеры препятствия
    /// </summary>
    /// <returns></returns>
    public Collider[] GetColliders()
    {
        return colliders;
    }

    private void Awake()
    {
        colliders = gameObject.GetComponentsInChildren<Collider>();
    }
}