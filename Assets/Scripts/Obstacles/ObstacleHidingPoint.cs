﻿using UnityEngine;

/// <summary>
/// Точка, расположенная рядом с каким-либо препятствием.
/// Используется в следующих задачах:
/// - поиск точки отступления врага, за которой можно спрятаться от атаки игрока
/// - поиск точки, где мог спрятаться игрок от врага
/// </summary>
public class ObstacleHidingPoint
{
    /// <summary>
    /// Препятствие, рядом с которым находится точка
    /// </summary>
    public IObstacle Obstacle { get; private set; }

    /// <summary>
    /// Координаты точки
    /// </summary>
    public Vector3 Point { get; private set; }

    /// <summary>
    /// Нормаль препятствия в данной точке
    /// </summary>
    public Vector3 Normal { get; private set; }

    public ObstacleHidingPoint(IObstacle obstacle, Vector3 point, Vector3 normal)
    {
        Obstacle = obstacle;
        Point = point;
        Normal = normal;
    }
}
