﻿using UnityEngine;

/// <summary>
/// Абстрактный класс отрисовщика, визуализирующего изменение уровня видимости объектов.
/// </summary>
public abstract class ChangingObstacleDrawer : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Включён ли отрисовщик")]
    protected bool isEnabled = true;

    [SerializeField]
    [Tooltip("Контроллер для управления уровнем видимости")]
    protected OpacityController opacityController = null;

    protected virtual void Awake()
    {
        opacityController.RegisterOpacityChangedListener(OnOpacityChanged);
    }

    protected virtual void OnDestroy()
    {
        if (opacityController == null)
        {
            return;
        }
        opacityController.UnregisterOpacityChangedListener(OnOpacityChanged);
    }

    /// <summary>
    /// Обновить отрисовку объектов по текущему уровню видимости
    /// </summary>
    protected abstract void Draw(float opacityValue);

    /// <summary>
    /// Обработчик события изменения уровня видимости объектов
    /// </summary>
    private void OnOpacityChanged()
    {
        if (isEnabled)
        {
            Draw(opacityController.OpacityValue);          
        }
    }
}
