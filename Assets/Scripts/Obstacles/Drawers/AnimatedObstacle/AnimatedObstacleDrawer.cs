using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ����������, ��������������� ��������� ������ ��������� ��������.
/// ��������� ������� ��������� �������� ���� ������������ �����������, �� ������� ���� ��������� Animation 
/// (��������, ����������/��������� �������).
/// ����������� ��� �������� ������ � OpacityController
/// </summary>
public class AnimatedObstacleDrawer : ChangingObstacleDrawer
{
    /// <summary>
    /// ���������, ����������� ����������� � ��� ���������
    /// </summary>
    private struct ObstacleAnimation
    {
        public ChangingObstacle Obstacle { get; private set; }
        public Animation Animation { get; private set; }

        public ObstacleAnimation(ChangingObstacle obstacle, Animation animation)
        {
            Obstacle = obstacle;
            Animation = animation;
        }
    }
    
    /// <summary>
    /// �������� ��������� ��������� �����������
    /// </summary>
    private readonly List<ObstacleAnimation> obstacleAnimations = new List<ObstacleAnimation>();

    /// <summary>
    /// �������� ��������� �������� �� �������� ������ ���������
    /// </summary>
    protected override void Draw(float opacityValue)
    {
        foreach (var obstacleAnimation in obstacleAnimations)
        {
            if (obstacleAnimation.Obstacle.InvertedChange)
            {
                opacityValue = OpacityController.MaxOpacityValue - opacityValue;
            }

            Animation animation = obstacleAnimation.Animation;
            animation[animation.clip.name].time = (1 - opacityValue * 0.01f) * animation.clip.length;
        }
    }

    private void Start()
    {
        foreach (var obstacle in ObstacleManager.Instance.ChangingObstacles)
        {
            if (obstacle.Transform.TryGetComponent(out Animation animation))
            {
                animation[animation.clip.name].time = obstacle.InvertedChange ? animation.clip.length : 0;
                animation[animation.clip.name].speed = 0;
                animation.Play();

                obstacleAnimations.Add(new ObstacleAnimation(obstacle, animation));
            }
        }
    }
}
