﻿using UnityEngine;

/// <summary>
/// Стратегия изменения видимости путем применения направленного эффекта Dissolve
/// </summary>
public class DirectionDissolveChangingStrategy: MaterialChangingStrategy
{
    private const float HighestDissolveOffset = float.MaxValue;

    [Tooltip("Минимальное значение смещения эффекта растворения")]
    [SerializeField]
    private float minDirectionDissolveOffset;
    
    [Tooltip("Максимальное значение смещения эффекта растворения")]
    [SerializeField]
    private float maxDirectionDissolveOffset;

    [Tooltip("Цвет границы эффекта растворения")]
    [SerializeField]
    [ColorUsageAttribute(true, true)]
    private Color dissolveEdgeColor;

    /// <summary>
    /// Текущее значение смещения эффекта растворения
    /// </summary>
    private float currentDissolveOffset;

    public float MinDirectionDissolveOffset => minDirectionDissolveOffset;
    public float MaxDirectionDissolveOffset => maxDirectionDissolveOffset;

    /// <summary>
    /// Установить значение видимости для материала
    /// </summary>
    /// <param name="material"></param>
    /// <param name="value"></param>
    public override void SetOpacity(Material material, float value)
    {
        currentDissolveOffset = value >= 1 ? HighestDissolveOffset :
            minDirectionDissolveOffset + (maxDirectionDissolveOffset - minDirectionDissolveOffset) * value;

        material.SetDirectionDissolve(currentDissolveOffset, dissolveEdgeColor);
    }

    /// <summary>
    /// Проверить, подходит ли материал для изменения по данной стратегии
    /// </summary>
    /// <param name="material"></param>
    /// <returns></returns>
    public override bool IsMaterialMatchStrategy(Material material)
    {
        return material.IsDissolvingMaterial();
    }
}
