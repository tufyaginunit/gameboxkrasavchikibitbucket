using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ����������, ��������������� ��������� ������ ��������� ��������.
/// ��������� ������� ��������� �������� ���� ��������� ���������� ��������� 
/// (��������, ����������� ��� ��������� ������ ������������).
/// ����������� ��� �������� ������ � OpacityController, 
/// ������� ������ ����� ��������� ��������� ������������ MaterialChangingStrategy
/// </summary>
public class MaterialChangingObstacleDrawer : ChangingObstacleDrawer
{   
    /// <summary>
    /// ��������� ��� ��������� ������ ���������
    /// </summary>
    private readonly List<OpacityChangingMaterial> opacityChangingMaterials = new List<OpacityChangingMaterial>();

    /// <summary>
    /// ��������� ��������� ���������
    /// </summary>
    private MaterialChangingStrategy materialChangingStrategy;

    /// <summary>
    /// �������� ��������� ������� �� �������� ������ ���������
    /// </summary>
    protected override void Draw(float opacityValue)
    {
        foreach (var item in opacityChangingMaterials)
        {
            item.SetOpacityValue(opacityValue);
        }
    }

    protected override void Awake()
    {
        base.Awake();

        materialChangingStrategy = GetComponent<MaterialChangingStrategy>();

        if (materialChangingStrategy == null)
        {
            Debug.LogWarning($"{name} - Missing obstacle material changing strategy");
            return;
        }
    }

    private void Start()
    {
        InitializeMaterials();
    }

    /// <summary>
    /// ���������������� ���������, ������� ����� ������������ ���������
    /// </summary>
    private void InitializeMaterials()
    {
        // ��� ����������� ������������ �������� sharedMaterials ��������� �����.
        // ��������� ���������� �� sharedMaterials ���� � ��������� ���������� � �������.
        // ����� ����� �� �����������, ����� ��������� ����� ���������� � �������� � ����.

        foreach (var obstacle in ObstacleManager.Instance.ChangingObstacles)
        {
            Renderer[] meshRenderers = obstacle.Transform.GetComponentsInChildren<Renderer>();

            foreach (var meshRenderer in meshRenderers)
            {
                MaterialChangingStrategy strategy = meshRenderer.GetComponentInParent<MaterialChangingStrategy>();
                if (strategy == null)
                {
                    strategy = materialChangingStrategy;
                }

                Material[] newSharedMaterials = new Material[meshRenderer.sharedMaterials.Length];
                for (int i = 0; i < meshRenderer.sharedMaterials.Length; i++)
                {
                    Material material = meshRenderer.sharedMaterials[i];
                    // �������� �� ����� ��������, ���� �� ������������� ��������� ���������.
                    // �� ����� ��������� ����� �������� � ������ ������������
                    if (!strategy.IsMaterialMatchStrategy(material))
                    {
                        newSharedMaterials[i] = material;
                        continue;
                    }

                    OpacityChangingMaterial opacityChangingMaterial = opacityChangingMaterials
                        .Find(m => m.InitialMaterial == material && m.OpacityChangingStrategy == strategy &&
                        m.InvertedChange == obstacle.InvertedChange);

                    if (opacityChangingMaterial == null)
                    {
                        opacityChangingMaterial = new OpacityChangingMaterial(material, strategy, 
                            obstacle.InvertedChange);
                        opacityChangingMaterials.Add(opacityChangingMaterial);
                    }
                    newSharedMaterials[i] = opacityChangingMaterial.ChangingMaterial;
                }
                meshRenderer.sharedMaterials = newSharedMaterials;
            }
        }
    }

}
