﻿using UnityEngine;
using UnityEngine.AI;
using System;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// Класс препятствия, меняющего значение своей видимости.
/// </summary>
public class ChangingObstacle : MonoBehaviour, IObstacle
{
    /// <summary>
    /// Класс, предоставляющий функционал для работы с коллайдерами изменяющихся препятствий
    /// </summary>
    protected static class ChangeableColliderFunc
    {
        /// <summary>
        /// Типы коллайдеров, которые подходят для изменяющихся препятствий
        /// </summary>
        public static readonly Type[] changeableColliderTypes =
        {
            typeof(BoxCollider),
            typeof(SphereCollider),
            typeof(CapsuleCollider),
            typeof(MeshCollider)
        };

        /// <summary>
        /// Проверить, является ли коллайдер изменяющимся
        /// </summary>
        /// <param name="collider"></param>
        /// <returns></returns>
        public static bool IsChangeableCollider(Collider collider)
        {
            return changeableColliderTypes.Contains(collider.GetType()) && IsConvex(collider);
        }

        /// <summary>
        /// Создать копию коллайдера на игровом объекте
        /// </summary>
        /// <param name="collider">Копируемый коллайдер</param>
        /// <param name="obj">Игровой объект, в который будет скопирован коллайдер</param>
        /// <returns></returns>
        public static Collider CreateCopyOnGameObject(Collider collider, GameObject obj)
        {
            Collider colliderCopy = null;
            if (collider.GetType() == typeof(BoxCollider))
            {
                colliderCopy = obj.AddComponent(collider as BoxCollider);
            }
            else if (collider.GetType() == typeof(SphereCollider))
            {
                colliderCopy = obj.AddComponent(collider as SphereCollider);
            }
            else if (collider.GetType() == typeof(CapsuleCollider))
            {
                colliderCopy = obj.AddComponent(collider as CapsuleCollider);
            }
            else if (collider.GetType() == typeof(MeshCollider))
            {
                colliderCopy = obj.AddComponent(collider as MeshCollider);
            }

            return colliderCopy;
        }

        /// <summary>
        /// Проверить, является ли коллайдер выпуклым
        /// </summary>
        /// <param name="collider"></param>
        /// <returns></returns>
        private static bool IsConvex(Collider collider)
        {
            return collider.GetType() != typeof(MeshCollider) ||
                (collider as MeshCollider).convex;
        }

    }

    [SerializeField]
    [Tooltip("Инвертировать процесс изменения прозрачности")]
    protected bool invertedChange = false;

    /// <summary>
    /// Параметры изменения видимости
    /// </summary>
    protected OpacityChangingParameters opacityChangingParameters;

    /// <summary>
    /// Коллайдеры для включения/отключения в зависимости от уровня видимости
    /// </summary>
    protected Collider[] opaqueColliders;

    /// <summary>
    /// Коллайдеры-триггеры, которые являются копиями opaqueColliders, и служат
    /// для детектирования препятствий, когда они являются полностью невидимыми
    /// </summary>
    protected Collider[] transparentColliders;

    /// <summary>
    /// Объект динамического препятствия для NavMesh.
    /// Служит для того, чтобы при появлении/исчезновении объекта автоматически пересчитывалась
    /// навигационная сетка для врагов
    /// </summary>
    protected NavMeshObstacle navMeshObstacle;

    /// <summary>
    /// Величина сдвига коллайдеров вниз, чтобы сквозь них можно было смотреть
    /// и стрелять
    /// </summary>
    protected float collidersShiftToLookAndAttack;

    /// <summary>
    /// Величина сдвига коллайдеров вниз, чтобы сквозь них можно было только смотреть, 
    /// но нельзя было пройти или стрелять
    /// </summary>
    protected float collidersShiftToLookBehindOnly;

    /// <summary>
    /// Текущий сдвиг коллайдеров по вертикали
    /// </summary>
    protected float currentCollidersShift;

    /// <summary>
    /// Верхняя точка коллайдеров объекта
    /// </summary>
    protected float collidersTopY;

    /// <summary>
    /// Текущее значение видимости данного препятствия (учитывает значние invertedChange)
    /// </summary>
    protected float currentOpacityValue;

    /// <summary>
    /// Трансформ препятствия
    /// </summary>
    public Transform Transform => transform;

    /// <summary>
    /// Тип препятствия
    /// </summary>
    public ObstacleType Type => ObstacleType.OpacityChanging;

    /// <summary>
    /// Текущее значение видимости препятствия
    /// </summary>
    public float OpacityValue => currentOpacityValue;

    /// <summary>
    /// Нужно ли инвертировать процесс изменения прозрачности
    /// </summary>
    public bool InvertedChange => invertedChange;

    /// <summary>
    /// Можно ли в данный момент пройти через препятствие
    /// </summary>
    /// <returns></returns>
    public bool IsPassable()
    {
        return currentOpacityValue <= 0;
    }

    /// <summary>
    /// Проверить, можно ли будет пройти через препятствие при указанном значении
    /// глобальной прозрачности
    /// </summary>
    /// <param name="globalOpacityValue"></param>
    /// <returns></returns>
    public bool CheckPassability(float globalOpacityValue)
    {
        return InvertedChange ? globalOpacityValue >= OpacityController.MaxOpacityValue :
            globalOpacityValue <= OpacityController.MinOpacityValue;
    }

    /// <summary>
    /// Является в данный момент препятствие полностью видимым
    /// </summary>
    /// <returns></returns>
    public bool IsOpaque()
    {
        return currentOpacityValue >= opacityChangingParameters.FullOpaqueValue;
    }

    /// <summary>
    /// Получить коллайдеры препятствия
    /// </summary>
    /// <returns></returns>
    public Collider[] GetColliders()
    {
        return opaqueColliders;
    }

    /// <summary>
    /// Инициализировать препятствие, задав параметры изменения видимости
    /// </summary>
    /// <param name="opacityChangingParameters"></param>
    public virtual void Initialize(OpacityChangingParameters opacityChangingParameters)
    {
        this.opacityChangingParameters = opacityChangingParameters;
        collidersShiftToLookAndAttack = collidersTopY - GameManager.Instance.GroundY - 0.1f;
        collidersShiftToLookBehindOnly = collidersTopY - GameManager.Instance.AttackPointY - 0.1f;
        currentCollidersShift = 0;
    }

    /// <summary>
    /// Установить уровень видимости препятствия
    /// </summary>
    /// <param name="valueInPercents">Уровень видимости в процентах</param>
    public virtual void SetOpacityValue(float opacityValueInPercents)
    {
        if (InvertedChange)
        {
            opacityValueInPercents = OpacityController.MaxOpacityValue - opacityValueInPercents;
        }
        SetCollidersState(opacityValueInPercents);
        currentOpacityValue = opacityValueInPercents;
    }

    protected virtual void Awake()
    {
        opaqueColliders = gameObject.GetComponentsInChildren<Collider>()
            .Where(c => ChangeableColliderFunc.IsChangeableCollider(c)).ToArray();

        CreateTransparentColliders();
        CreateNavMeshObstacle();
    }

    /// <summary>
    /// Создать невидимые коллайдеры-триггеры
    /// </summary>
    private void CreateTransparentColliders()
    {
        // Найдём наибольшую точку коллайдеров. 
        // Коллайдеры, через которые можно видеть и стрелять, будут точно такими же, как и 
        // исходные, но опущенные вниз так, что персонажи будут возвышаться над ними.
        float topY = float.MinValue;
        foreach (var item in opaqueColliders)
        {
            float topColliderY = item.bounds.center.y + item.bounds.extents.y;
            float bottomColliderY = item.bounds.center.y - item.bounds.extents.y;
            if (topColliderY > topY)
            {
                topY = topColliderY;
            }
        }

        collidersTopY = topY;
 
        transparentColliders = new Collider[opaqueColliders.Length];

        // Скопируем каждый коллайдер на новый объект
        for (int i = 0; i < opaqueColliders.Length; i++)
        {
            Collider collider = opaqueColliders[i];

            GameObject obj = new GameObject($"{collider.name}_TransparentCollider");

            obj.transform.position = collider.transform.position;
            obj.transform.rotation = collider.transform.rotation;
            obj.transform.localScale = collider.transform.lossyScale;
            obj.transform.SetParent(transform);
            obj.layer = transform.gameObject.layer;

            transparentColliders[i] = ChangeableColliderFunc.CreateCopyOnGameObject(collider, obj);          
        }
    }

    /// <summary>
    /// Создать компонент NavMesh препятствия для влияния прозрачности препятствия 
    /// на навигационную сетку врагов.
    /// </summary>
    private void CreateNavMeshObstacle()
    {
        // Не добавляем компонент NavMeshObstacle, если он уже есть
        if (TryGetComponent(out NavMeshObstacle _))
        {
            return;
        }

        if (!TryGetComponent(out BoxCollider collider))
        {
            return;
        }

        navMeshObstacle = gameObject.AddComponent<NavMeshObstacle>();
        navMeshObstacle.size = collider.size;
        navMeshObstacle.center = collider.center;
        navMeshObstacle.carving = true;
        navMeshObstacle.carveOnlyStationary = true;
        navMeshObstacle.carvingMoveThreshold = 1;
        navMeshObstacle.carvingTimeToStationary = 1;
        navMeshObstacle.shape = NavMeshObstacleShape.Box;
    }

    /// <summary>
    /// Включить/выключить коллайдеры
    /// </summary>
    /// <param name="colliders">Коллайдеры</param>
    /// <param name="enable">true - включить коллайдеры, false - выключить коллайдеры</param>
    private void SetEnabledColliders(Collider[] colliders, bool enabled)
    {
        foreach (var collider in colliders)
        {
            collider.isTrigger = !enabled;
        }

        if (navMeshObstacle != null)
        {
            navMeshObstacle.enabled = enabled;
        }
        
    }

    /// <summary>
    /// Установить состояние коллайдеров, в зависимости от значения видимости
    /// </summary>
    /// <param name="opacityValueInPercents"></param>
    private void SetCollidersState(float opacityValueInPercents)
    {        
        SetEnabledColliders(opaqueColliders,
            opacityValueInPercents >= opacityChangingParameters.FullOpaqueValue);

        SetEnabledColliders(transparentColliders, opacityValueInPercents > 0);

        // Коллайдеры выключены, манипуляции с их высотой делать бессмысленно
        if (opacityValueInPercents <= 0)
        {
            return;
        }

        if (opacityValueInPercents > opacityChangingParameters.AttackValue)
        {
            SetCollidersShift(collidersShiftToLookBehindOnly);
        }
        else
        {
            SetCollidersShift(collidersShiftToLookAndAttack);
        }
    }

    /// <summary>
    /// Осуществить сдвиг коллайдеров объекта по вертикали, чтобы можно было смотреть и/или
    /// стрелять через них
    /// </summary>
    /// <param name="collidersShift">Величина сдвига</param>
    private void SetCollidersShift(float collidersShift)
    {
        if (collidersShift == currentCollidersShift)
        {
            return;
        }

        foreach (var collider in transparentColliders)
        {
            Vector3 shiftedPosition = collider.transform.position;
            
            shiftedPosition.y += currentCollidersShift - collidersShift;

            collider.transform.position = shiftedPosition;
        }
        currentCollidersShift = collidersShift;
    }

}
