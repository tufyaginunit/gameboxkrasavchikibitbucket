using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// �������� �����������.
/// � ������ ���� ������� ��� ����� ������ �����������, �� ������� ���� ������������ 
/// ����� ���������� �� ����� ������
/// ��������-�����. ������������� ��������� ��� ��������� �������� ������ ������� GameManager
/// </summary>
public class ObstacleManager : MonoBehaviour
{
    [SerializeField]
    [Tooltip("��������/��������� ���������� ��������� ����� �����������")]
    private bool drawHidingPoints = true;

    /// <summary>
    /// ��������� �� ������� ����������� �� �����, �� ������� ���� ����� ���������
    /// </summary>
    const float HidingPointDistanceFromWall = 1;

    /// <summary>
    /// ������� ����������, �������� ����������� ��������� ����� ����� ��������� 
    /// ������� ����������� (��� �����������)
    /// </summary>
    const float ReducingHidingPointSqrDistance = 0.5f;

    /// <summary>
    /// ��� �������-����������� �� �����
    /// </summary>
    private IObstacle[] obstacles;

    /// <summary>
    /// �������-�����������, ��������� ������ ������� ����� ���������
    /// </summary>
    private ChangingObstacle[] changingObstacles;

    /// <summary>
    /// ��� ��������� ����� �����������
    /// </summary>
    private ObstacleHidingPoint[] hidingPoints;

    public static ObstacleManager Instance { get; private set; }

    public IObstacle[] Obstacles => obstacles;

    public ChangingObstacle[] ChangingObstacles => changingObstacles;

    public ObstacleHidingPoint[] HidingPoints => hidingPoints;
    
    public bool DrawHidingPoints => drawHidingPoints;

    /// <summary>
    /// �������� ����������� �� ������ ����� �����������
    /// </summary>
    /// <param name="hidingPoints"></param>
    /// <returns></returns>
    public List<IObstacle> GetHidingPointsObstacles(List<ObstacleHidingPoint> hidingPoints)
    {
        List<IObstacle> obstacles = new List<IObstacle>();
        foreach (var point in hidingPoints)
        {
            if (!obstacles.Contains(point.Obstacle))
            {
                obstacles.Add(point.Obstacle);
            }
        }
        return obstacles;
    }

    /// <summary>
    /// �������� �����, ������� ��������� ����� ������ ��������� �����������,
    /// ���� �������� �� ��������� �����
    /// </summary>
    /// <param name="pointOfViewToHideFrom">�����, � ������� ���������� ������ �������</param>
    /// <param name="obstacles">������ �����������</param>
    /// <returns></returns>
    public List<ObstacleHidingPoint> GetPointsToHideFromPointOfView(Vector3 pointOfViewToHideFrom,
        List<IObstacle> obstacles)
    {
        List<ObstacleHidingPoint> savePoints = new List<ObstacleHidingPoint>();

        foreach (var obstacle in obstacles)
        {
            Collider[] colliders = obstacle.GetColliders();
            foreach (var collider in colliders)
            {
                if (TryGetPointBehindCollider(collider, obstacle, pointOfViewToHideFrom,
                    out ObstacleHidingPoint pointBehindCollider))
                {
                    savePoints.Add(pointBehindCollider);
                }
            }
        }

        return savePoints;
    }

    /// <summary>
    /// �������� �����, ������� ��������� ������ ��������� �����������
    /// </summary>
    /// <param name="obstacles">������ �����������</param>
    /// <returns></returns>
    public List<ObstacleHidingPoint> GetObstaclesHidingPoints(List<IObstacle> obstacles)
    {
        return hidingPoints.Where(p => obstacles.Contains(p.Obstacle)).ToList();
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        // ������� ��� �����������
        // TODO: ����� ��������������, ������ ����� �� �� ���� ������� ��������,
        // � ������ �� �������� � ����� ����������� GetComponentInChildren
        obstacles = FindObjectsOfType<MonoBehaviour>().OfType<IObstacle>().ToArray();
        
        List<ChangingObstacle> changingObstaclesList = new List<ChangingObstacle>();
        foreach (var item in obstacles)
        {
            if (item.GetType() == typeof(ChangingObstacle))
            {
                changingObstaclesList.Add(item as ChangingObstacle);
            }
        }

        changingObstacles = changingObstaclesList.ToArray();
    }

    private void Start()
    {       
        hidingPoints = CalculateObstaclesHidingPoints(obstacles);
    }

    private void OnDrawGizmos()
    {
        // ���������� ��������� ���� ������������� ����� ��� �����������
        if (Instance == null || !drawHidingPoints || hidingPoints == null)
        {
            return;
        }

        foreach (var item in hidingPoints)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawCube(item.Point, new Vector3(0.2f, 0.2f, 0.2f));
        }
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }

    /// <summary>
    /// �������� ������ ����� ������ ������ ���������� �����������
    /// </summary>
    /// <param name="collider"></param>
    /// <param name="obstacle"></param>
    /// <returns></returns>
    private List<ObstacleHidingPoint> GetColliderHidingPoints(Collider collider, IObstacle obstacle)
    {
        const float AngleStep = 10f;
        const float MaxAngle = 360;

        float groundY = GameManager.Instance.GroundY;

        List<ObstacleHidingPoint> savePoints = new List<ObstacleHidingPoint>();

        Vector3 center = collider.bounds.center;

        float currentAngle = 0;
        while (currentAngle < MaxAngle)
        {
            Vector3 vectorFromCenter = VectorFunc.GetRotatedUnitVector(currentAngle);

            Vector3 outsidePoint = center + VectorFunc.MaxRaycastDistance * vectorFromCenter;

            Ray ray = new Ray(outsidePoint, -vectorFromCenter);
            if (collider.Raycast(ray, out RaycastHit hitInfo, VectorFunc.MaxRaycastDistance))
            {
                Vector3 normalXZ = Vector3.Scale(hitInfo.normal, new Vector3(1, 0, 1)).normalized;
                if (Vector3.Dot(normalXZ, vectorFromCenter) <= 0)
                {
                    normalXZ = vectorFromCenter;
                }
                Vector3 savePoint = GetPointOutsideColliderInDirection(hitInfo.point,
                    normalXZ, groundY);
                savePoint.y = groundY;
                savePoint += normalXZ * HidingPointDistanceFromWall;

                if (IsPointOutsideAllObstacles(savePoint))
                {
                    savePoints.Add(new ObstacleHidingPoint(obstacle, savePoint, normalXZ));
                }
            }
            currentAngle += AngleStep;
        }

        return savePoints;
    }

    /// <summary>
    /// ���������� �������� �����, ����������� ����� �� �����������, ���� �������� �� ���������
    /// �� ��������� �����
    /// </summary>
    /// <param name="collider">���������</param>
    /// <param name="obstacle">�����������, � �������� ��������� ���������</param>
    /// <param name="pointOfViewToHideFrom">�����, � ������� ���������� ������ �������</param>
    /// <param name="pointBehindCollider">��������� ����� �� �����������</param>
    /// <returns></returns>
    private bool TryGetPointBehindCollider(Collider collider, IObstacle obstacle,
        Vector3 pointOfViewToHideFrom, out ObstacleHidingPoint pointBehindCollider)
    {
        pointBehindCollider = null;
        float groundY = GameManager.Instance.GroundY;

        Vector3 center = collider.bounds.center;
        center.y = pointOfViewToHideFrom.y;
        Vector3 vectorFromCenter = center - pointOfViewToHideFrom;
        vectorFromCenter = vectorFromCenter.normalized;

        Vector3 outsidePoint = center + VectorFunc.MaxRaycastDistance * vectorFromCenter;

        Ray ray = new Ray(outsidePoint, -vectorFromCenter);
        if (collider.Raycast(ray, out RaycastHit hitInfo, VectorFunc.MaxRaycastDistance))
        {           
            Vector3 savePoint = hitInfo.point + vectorFromCenter * HidingPointDistanceFromWall;
            savePoint.y = groundY;

            pointBehindCollider = new ObstacleHidingPoint(obstacle, savePoint, vectorFromCenter);
        }

        return pointBehindCollider != null;
    }

    /// <summary>
    /// �������� ����� ������� ���������� � ��������� �����������
    /// </summary>
    /// <param name="pointInsideCollider">����� ������ ����������</param>
    /// <param name="direction">�����������, ����� �������� ���� �����</param>
    /// <param name="groundY">������� �����</param>
    /// <returns></returns>
    private Vector3 GetPointOutsideColliderInDirection(Vector3 pointInsideCollider, 
        Vector3 direction, float groundY)
    {
        bool isPointOutsideFound = false;
        Vector3 pointOutsideCollider = pointInsideCollider;
        while (!isPointOutsideFound)
        {
            pointOutsideCollider += direction * 0.5f;
            Vector3 topPoint = pointOutsideCollider;
            topPoint.y = VectorFunc.MaxRaycastDistance;
            isPointOutsideFound = !Physics.Raycast(topPoint, Vector3.down, out _,
                VectorFunc.MaxRaycastDistance - groundY, 
                GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore);
        }
        return pointOutsideCollider;
    }

    /// <summary>
    /// ��������� ��������, �� ������������� �� ����� ������-���� �������������
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    private bool IsPointOutsideAllObstacles(Vector3 point)
    {
        Vector3 topPoint = point;
        topPoint.y = VectorFunc.MaxRaycastDistance;
        return !Physics.Raycast(topPoint, Vector3.down, topPoint.y - point.y, 
            GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore);
    }

    /// <summary>
    /// ���������� ����� ����������� ������ �����������
    /// </summary>
    /// <param name="obstacles">������ �����������</param>
    /// <returns></returns>
    private ObstacleHidingPoint[] CalculateObstaclesHidingPoints(IObstacle[] obstacles)
    {
        List<ObstacleHidingPoint> savePoints = new List<ObstacleHidingPoint>();

        foreach (var obstacle in obstacles)
        {
            Collider[] colliders = obstacle.GetColliders();
            foreach (var collider in colliders)
            {
                List<ObstacleHidingPoint> colliderSavePoints = GetColliderHidingPoints(collider, obstacle);
                foreach (var point in colliderSavePoints)
                {
                    savePoints.Add(point);
                }
            }
        }

        ReducePointsCount(savePoints, ReducingHidingPointSqrDistance);
        return savePoints.ToArray();
    }

    /// <summary>
    /// ��������� ���������� ������������� ����� �����������, ������ ��,
    /// ������� ��������� ������ ���� � �����, � �������� ������������
    /// </summary>
    /// <param name="hidingPoints"></param>
    /// <param name="minSqrDistance"></param>
    private void ReducePointsCount(List<ObstacleHidingPoint> hidingPoints, float minSqrDistance)
    {
        NavMeshPath navMeshPath = new NavMeshPath();

        for (int i = 0; i < hidingPoints.Count; i++)
        {
            if (hidingPoints[i] == null)
            {
                continue;
            }

            for (int j = i + 1; j < hidingPoints.Count; j++)
            {
                // �� ������� �����, ���� ��� ��������� � ������ ����� �����������
                if (hidingPoints[j] == null || 
                    hidingPoints[i].Obstacle.Type != hidingPoints[j].Obstacle.Type)
                {
                    continue;
                }
                if (Vector3.SqrMagnitude(hidingPoints[i].Point - hidingPoints[j].Point) < minSqrDistance)
                {
                    hidingPoints[j] = null;
                }
            }

            // ������� �����, �� ������� ���������� ����� (��� �������� ��� NavMeshSurface)
            if (!NavMesh.CalculatePath(hidingPoints[i].Point, hidingPoints[i].Point, 
                NavMesh.AllAreas, navMeshPath))
            {
                hidingPoints[i] = null;
            }
        }      

        // ������� ���������� ��� �������� �����
        for (int i = hidingPoints.Count - 1; i >= 0; i--)
        {
            if (hidingPoints[i] == null)
            {
                hidingPoints.RemoveAt(i);
            }
        }
    }

}
