using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

/// <summary>
/// ����������� �����, �������������� ��������� ������������ ������. 
/// �� ���� ��������� ���������� ���������� (��������, �����, ����).
/// ������������� ����� ������ ����������� �� �������, ������� ������ ����� ����� �������� 
/// � ������� ����� ����������.
/// ���� ����� ������� ������������� ����� ������������ ������, ��� ����� ������������
/// �� ������� ������.
/// </summary>
public abstract class DamageableObject : MonoBehaviour, IDamageable, ISaveable
{
    [SerializeField]
    [Tooltip("��������� (������������) ��������")]
    protected int startHealth = 100;

    [SerializeField]
    [Tooltip("�������� ��� ��������� �����������")]
    protected bool regenerationEnabled = false;

    [SerializeField]
    [Tooltip("����� ����������� ������ ���������")]
    protected float oneHitRegenerationTime = 1;

    [SerializeField]
    [Tooltip("������ ��� ���������")]
    protected VisualEffect hitVisualEffect;

    [SerializeField]
    [Tooltip("������ ��� ���������")]
    protected VisualEffect healVisualEffect;

    [SerializeField]
    [Tooltip("�������� �� ������ �����������")]
    protected bool immortal = false;

    /// <summary>
    /// �������� �������� �����������
    /// </summary>
    protected Coroutine regenerationCoroutine = null;

    /// <summary>
    /// ������� �������� HP
    /// </summary>
    public int Health { get; private set; }

    /// <summary>
    /// ������������ �������� HP
    /// </summary>
    public int MaxHealth => startHealth;

    /// <summary>
    /// Transform �������
    /// </summary>
    public Transform Transform => transform;

    /// <summary>
    /// ������� ������, ���������� ��� ���������� HP �� ����
    /// </summary>
    private event Action Killed;

    /// <summary>
    /// ������� �����������, ���������� ��� ����������� ������ � ����������� �����
    /// </summary>
    private event Action Revived;

    /// <summary>
    /// ���������, ��� �� ������
    /// </summary>
    /// <returns></returns>
    public virtual bool IsAlive()
    {
        return Health > 0;
    }

    /// <summary>
    /// ���������� ��������� �����
    /// </summary>
    /// <param name="damageValue">���������� �����</param>
    public virtual void ReceiveDamage(int damageValue)
    {
        if (immortal)
        {
            return;
        }

        if (Health <= 0)
        {
            return;
        }

        SetHealth(Health - damageValue);

        if (Health <= 0)
        {
            Killed?.Invoke();
        }
    }

    /// <summary>
    /// �������� HP
    /// </summary>
    /// <param name="healthToAdd">���������� HP ��� ����������</param>
    public void AddHealth(int healthToAdd)
    {
        if (healthToAdd < 0)
        {
            return;
        }

        SetHealth(Health + healthToAdd);
    }

    /// <summary>
    /// ������ ������� ��� ��������� ����� �������
    /// </summary>
    public virtual void HitEffect()
    {
        if (hitVisualEffect != null)
        {
            hitVisualEffect.Play();
        }
    }

    /// <summary>
    /// ������ ������� ��� ������� �������
    /// </summary>
    public virtual void HealEffect()
    {
        if (healVisualEffect != null)
        {
            healVisualEffect.Play();
        }
    }

    /// <summary>
    /// ��������/��������� �����������
    /// </summary>
    /// <param name="enabled"></param>
    public virtual void SetEnabledRegeneration(bool enabled)
    {
        if (!regenerationEnabled || enabled && !IsAlive())
        {
            return;
        }
        this.StopAndNullCoroutine(ref regenerationCoroutine);
        if (enabled)
        {
            regenerationCoroutine = StartCoroutine(PerformRegeneration());
        }
    }

    /// <summary>
    /// �������� ��������� ������� ������ �������
    /// </summary>
    /// <param name="listener"></param>
    public virtual void RegisterKilledListener(Action listener)
    {
        Killed += listener;
    }

    /// <summary>
    /// ������� ��������� ������� ������ �������
    /// </summary>
    /// <param name="listener"></param>
    public virtual void UnregisterKilledListener(Action listener)
    {
        Killed -= listener;
    }

    /// <summary>
    /// �������� ��������� ������� ����������� �������
    /// </summary>
    /// <param name="listener"></param>
    public virtual void RegisterRevivedListener(Action listener)
    {
        Revived += listener;
    }

    /// <summary>
    /// ������� ��������� ������� ����������� �������
    /// </summary>
    /// <param name="listener"></param>
    public virtual void UnregisterRevivedListener(Action listener)
    {
        Revived -= listener;
    }

    protected virtual void Awake()
    {
        SetStartHealth();
        if (regenerationEnabled)
        {
            SetEnabledRegeneration(true);
        }
    }

    protected virtual void Destroy()
    {
    }

    protected virtual void Start()
    {
    }

    /// <summary>
    /// ���������� �������� HP
    /// </summary>
    /// <param name="healthValue">�������� HP</param>
    protected virtual void SetHealth(int healthValue)
    {
        Health = healthValue;
        if (Health <= 0)
        {
            Health = 0;
        }
        else if (Health > MaxHealth)
        {
            Health = MaxHealth;
        }
    }

    /// <summary>
    /// ��������� ������������ �������� 
    /// </summary>
    public virtual void SetStartHealth()
    {
        Health = startHealth;
    }

    /// <summary>
    /// ��������, ����������� ����������� ����������� HP
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator PerformRegeneration()
    {
        float timeToNextRegeneration = Time.time + oneHitRegenerationTime;
        while (true)
        {
            if (IsAlive() && Time.time > timeToNextRegeneration)
            {
                if (Health < MaxHealth)
                {
                    AddHealth(1);
                }
                timeToNextRegeneration = Time.time + oneHitRegenerationTime;
            }

            yield return null;
        }
    }

    /// <summary>
    /// ������ ��������� �������. 
    /// ������������ ��� �������������� ��������� ������� ��� ����������� �� ����������� �����.
    /// ������������ ����� ����� ���� ����������� ��� �������� �� �����,
    /// �� ������ �� ���������� ��� ������ ��� ����, ����� ��� ������ ���� � ����������� �����
    /// ������������ �������������� ��������� �������
    /// </summary>
    /// <param name="state"></param>
    public void SetState(object state)
    {
        Health = startHealth;
        Revived?.Invoke();
    }

    /// <summary>
    /// �������� ��������� �������.
    /// ������������ ����� ����� ���� ����������� ��� ���������� ������� � ����,
    /// ������ ��� �� ���������, ������� ���������� ������ ������ � �������� ���������
    /// </summary>
    /// <returns></returns>
    public object GetState()
    {
        return "";
    }
}
