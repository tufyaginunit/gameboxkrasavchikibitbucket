using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// ��������-�������� ��������, ����������� ���� ������
/// </summary>
public class FieldOfViewManager : MonoBehaviour
{
    /// <summary>
    /// ������ ����� ������ ���� ������
    /// </summary>
    private readonly List<FieldOfView> fieldsOfView = new List<FieldOfView>();

    public static FieldOfViewManager Instance { get; private set; }
    
    public List<FieldOfView> FieldsOfView => fieldsOfView;

    /// <summary>
    /// �������� ��������� �������� ������� �� ������ �� ��������� ���������
    /// </summary>
    /// <param name="startPoint">��������� ����� �������</param>
    /// <param name="direction">����������� �������</param>
    /// <param name="distance">����� �������</param>
    /// <returns></returns>
    public static RaycastHit GetFovRaycastHit(Vector3 startPoint, Vector3 direction, float distance)
    {
        if (Physics.Raycast(startPoint, direction, out RaycastHit hitInfo, distance, 
            GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore))
        {
            return hitInfo;
        }
        else
        {
            return new RaycastHit();
        }
    }

    /// <summary>
    /// �������� ������ ���� ������ ����� � ����� ������
    /// </summary>
    /// <param name="fov"></param>
    public void AddFieldOfView(FieldOfView fov)
    {
        if (!fieldsOfView.Contains(fov))
        {
            fieldsOfView.Add(fov);
        }       
    }

    /// <summary>
    /// ������� ������ ���� ������ ����� �� ������
    /// </summary>
    /// <param name="fov"></param>
    public void RemoveFieldOfView(FieldOfView fov)
    {
        fieldsOfView.Remove(fov);
    }

    private void Awake()
    {        
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }
}
