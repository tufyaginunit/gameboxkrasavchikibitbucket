using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// ������������ ���� ������ ������ � PlayMode
/// </summary>
[ExecuteInEditMode]
public class FieldOfViewVisualizer : MonoBehaviour
{
    /// <summary>
    /// ������ ������� �� ������ ���� ������ �� ��������� �����
    /// </summary>
    public struct ViewCastInfo
    {
        /// <summary>
        /// ��������� �� ������������ � ������������
        /// </summary>
        public bool hit;

        /// <summary>
        /// �������� ����� ������� ���� ������
        /// </summary>
        public Vector3 point;

        /// <summary>
        /// ����� ������� ���� ������
        /// </summary>
        public float dist;

        /// <summary>
        /// ���� ������� ���� ������
        /// </summary>
        public float angle;

        public ViewCastInfo(bool hit, Vector3 point, float dist,
            float angle)
        {
            this.hit = hit;
            this.point = point;
            this.dist = dist;
            this.angle = angle;
        }
    }

    /// <summary>
    /// ���������� �������� �� ��������� ������� �����������
    /// </summary>
    const int EdgeResolveIterationCount = 6;

    /// <summary>
    /// ������� ����� ������� �������� ��������, ��� ������� �������,
    /// ��� ����� �������� ��������� ������ �������
    /// </summary>
    const float EdgeDistanceThreshold = 0.5f;

    /// <summary>
    /// ��� �� ��������� ����� ��������� ���� ������ � ��������
    /// </summary>
    const float DefaultLinesStepInDegrees = 10f;

    [SerializeField]
    [Tooltip("��� ����� ��������� ���� ������ � ��������. ������� �������� ��������� ��������" +
        "���������� ������, �� ����������� ������������������")]
    [Range(1, 20)]
    private float linesStepInDegrees = DefaultLinesStepInDegrees;

    [SerializeField]
    [Tooltip("�������� ���� ������ ����� � ��������� ���������")]
    private Material fovMaterial;

    [SerializeField]
    [Tooltip("�������� ���� ������ ����� � ��������� �������")]
    private Material fovDetectedMaterial;

    /// <summary>
    /// ��� ����� ��������� ���� ������ � �������� (����������� ����, ������� ����� �������� ����� �� ������)
    /// </summary>
    public static float LinesStepInDegrees = DefaultLinesStepInDegrees;

    /// <summary>
    /// �������� ������� ���� ������ (�������, �������� �������� �� ������ FOV, ����� ����� �����������)
    /// </summary>
    /// <param name="fovRotation"></param>
    /// <param name="fowAngle"></param>
    /// <returns></returns>
    public static List<Vector3> GetFovVectors(float fovRotation, float fowAngle)
    {
        List<Vector3> fovLines = new List<Vector3>();

        float currentAngle = -fowAngle / 2;
        while (currentAngle <= fowAngle / 2)
        {
            Vector3 fovEdgeVector = VectorFunc.GetRotatedUnitVector(fovRotation + currentAngle);
            fovLines.Add(fovEdgeVector);

            currentAngle += LinesStepInDegrees;
        }
        return fovLines;
    }

    /// <summary>
    /// �������� ������� �������� ���� ������
    /// </summary>
    /// <param name="fovCenter"></param>
    /// <param name="fovRotation"></param>
    /// <param name="fowAngle"></param>
    /// <param name="fovDistance"></param>
    /// <returns></returns>
    public static List<Vector3> GetFovEdgePointsWithObstacleInfluence(
        Vector3 fovCenter, float fovRotation, float fowAngle, float fovDistance)
    {

        List<Vector3> fovLines = new List<Vector3>();
        ViewCastInfo newViewCastInfo = new ViewCastInfo();
        ViewCastInfo oldViewCastInfo = new ViewCastInfo();

        float minAngle = fovRotation - fowAngle / 2;
        float maxAngle = fovRotation + fowAngle / 2;
        float currentAngle = minAngle;

        bool isFirstIteration = true;
        while (currentAngle < maxAngle)
        {
            newViewCastInfo = ViewCast(fovCenter, currentAngle, fovDistance);
            currentAngle += LinesStepInDegrees;

            if (!isFirstIteration)
            {
                bool isThresholdEsceeded =
                    Mathf.Abs(newViewCastInfo.dist - oldViewCastInfo.dist) > EdgeDistanceThreshold;

                if (newViewCastInfo.hit != oldViewCastInfo.hit ||
                    newViewCastInfo.hit && oldViewCastInfo.hit && isThresholdEsceeded)
                {
                    newViewCastInfo = FindEdge(fovCenter, fovDistance, oldViewCastInfo,
                        newViewCastInfo);
                    currentAngle = newViewCastInfo.angle;
                }
            }

            fovLines.Add(newViewCastInfo.point);

            isFirstIteration = false;
            oldViewCastInfo = newViewCastInfo;
        }
        newViewCastInfo = ViewCast(fovCenter, maxAngle, fovDistance);
        fovLines.Add(newViewCastInfo.point);

        return fovLines;
    }

    /// <summary>
    /// ���������� ��������� �������, ���������� ��� ����������� ����� �� ������ ���� ������
    /// </summary>
    /// <param name="fovCenter"></param>
    /// <param name="globalAngle"></param>
    /// <param name="fovDistance"></param>
    /// <returns></returns>
    private static ViewCastInfo ViewCast(Vector3 fovCenter, float globalAngle, float fovDistance)
    {
        Vector3 fovEdgeVector = VectorFunc.GetRotatedUnitVector(globalAngle);

        RaycastHit hitInfo = FieldOfViewManager.GetFovRaycastHit(fovCenter, fovEdgeVector, fovDistance);
        if (hitInfo.collider != null)
        {
            return new ViewCastInfo(true, hitInfo.point, hitInfo.distance, globalAngle);
        }
        else
        {
            return new ViewCastInfo(false, fovCenter + fovEdgeVector * fovDistance, fovDistance, globalAngle);
        }
    }

    /// <summary>
    /// �������� ������� �������
    /// </summary>
    /// <param name="fovCenter"></param>
    /// <param name="fovDistance"></param>
    /// <param name="minViewCast"></param>
    /// <param name="maxViewCast"></param>
    /// <returns></returns>
    private static ViewCastInfo FindEdge(Vector3 fovCenter, float fovDistance, 
        ViewCastInfo minViewCast, ViewCastInfo maxViewCast)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        ViewCastInfo newViewCast = new ViewCastInfo();

        for (int i = 0; i < EdgeResolveIterationCount; i++)
        {
            float angle = (minAngle + maxAngle) * .5f;
            newViewCast = ViewCast(fovCenter, angle, fovDistance);

            bool isThresholdEsceeded =
               Mathf.Abs(newViewCast.dist - minViewCast.dist) > EdgeDistanceThreshold;

            // ����� �������� ������ ������ �������
            if (newViewCast.hit == minViewCast.hit && !isThresholdEsceeded)
            {
                break;
            }
            else
            {
                maxAngle = angle;
            }
        }
        return newViewCast;

    }

    [ExecuteInEditMode]
    private void Update()
    {
        LinesStepInDegrees = linesStepInDegrees;
    }

    private void OnEnable()
    {
        RenderPipelineManager.endCameraRendering += OnEndCameraRendering;
    }

    private void OnDisable()
    {
        RenderPipelineManager.endCameraRendering -= OnEndCameraRendering;
    }

    private void OnPostRender()
    {
        DrawAllFovs();
    }

    private void OnEndCameraRendering(ScriptableRenderContext context, Camera camera)
    {
        OnPostRender();
    }

    /// <summary>
    /// ��������������� ��� ������� ���� ������
    /// </summary>
    private void DrawAllFovs()
    {
        if (FieldOfViewManager.Instance == null)
        {
            return;
        }
        List<FieldOfView> fieldsOfView = FieldOfViewManager.Instance.FieldsOfView;
        if (fieldsOfView == null)
        {
            return;
        }

        foreach (var fov in FieldOfViewManager.Instance.FieldsOfView)
        {
            DrawFov(fov);
        }
    }

    /// <summary>
    /// ��������������� ���� ������
    /// </summary>
    /// <param name="fov"></param>
    private void DrawFov(FieldOfView fov) 
    {
        Vector3 fovCenter = fov.PointOfSight.transform.position;
        float fovRotation = fov.PointOfSight.transform.eulerAngles.y;

        // �������� ������� ��� ��������� ���� ������
        List<Vector3> fovPoints = GetFovEdgePointsWithObstacleInfluence(fovCenter, fovRotation, 
            fov.Angle, fov.Distance);

        Material currentMaterial = fov.DetectingState != FieldOfView.TargetDetectingState.NotDetected ?
            fovDetectedMaterial : fovMaterial;

        currentMaterial.SetPass(0);

        GL.Flush();
        GL.PushMatrix();
        GL.MultMatrix(transform.localToWorldMatrix);
        GL.Begin(GL.TRIANGLES);
        for (int i = 1; i < fovPoints.Count; i++)
        {
            GL.Vertex(fovCenter);
            GL.Vertex(fovPoints[i - 1]);
            GL.Vertex(fovPoints[i]);
        }
        GL.End();
        GL.PopMatrix();
    }

}
