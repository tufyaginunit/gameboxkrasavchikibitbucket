using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ������������ ������, �������������� �����.
/// ����������� ��������������� �� ������ �����, ��������� ���� �� ����������� HP,
/// � ���������� ��������� �������� ��� �����������
/// </summary>
public class Enemy : DamageableObject
{
    protected override void Start()
    {
        EnemyManager.Instance.AddEnemy(this);
    }
    protected override void Destroy()
    {
        EnemyManager.Instance.RemoveEnemy(this);
    }
}
