﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Эффект смерти врага-призрака.
/// Чтобы визуальный эффект сработал, необходимо, чтобы для врага 
/// в качестве материала был назначен шейдер dissolve_direction
/// Рекомендуется добавлять на отдельный дочерний объект врага.
/// </summary>
public class GhostDeathEffect : DeathEffect
{
    [SerializeField]
    [Tooltip("Начальное смещение для эффекта растворения")]
    private Vector3 dissolveStartOffset;

    [SerializeField]
    [Tooltip("Конечное смещение для эффекта растворения")]
    private Vector3 dissolveEndOffset;

    /// <summary>
    /// Материалы призрака
    /// </summary>
    private List<Material> materials;
    private Vector3 dissolveChangeDirection;
    private float dissolveChangeVectorLength;

    /// <summary>
    /// Обработать событие смерти призрака
    /// </summary>
    protected override void ProcessDeath()
    {
        base.ProcessDeath();
        StartCoroutine(PerformDeath(deathDuration));
    }

    protected override void Awake()
    {
        base.Awake();
        // Добавляем в список все материалы объекта и дочерних объектов для последующего изменения
        // их прозрачности
        Renderer[] meshRenderers = damageableObject.Transform.GetComponentsInChildren<Renderer>();
        materials = new List<Material>();
        foreach (var meshRenderer in meshRenderers)
        {
            foreach (var material in meshRenderer.materials)
            {
                materials.Add(material);
                material.SetDirectionDissolve(dissolveStartOffset);
            }
        }
        Vector3 dissolveChangeVector = dissolveEndOffset - dissolveStartOffset;
        dissolveChangeVectorLength = dissolveChangeVector.magnitude;
        dissolveChangeDirection = dissolveChangeVector / dissolveChangeVectorLength;
    }

    /// <summary>
    /// Корутина, выполняющая плавное растворение врага
    /// </summary>
    /// <returns></returns>
    private IEnumerator PerformDeath(float deathDuration)
    {
        float changeSpeed = dissolveChangeVectorLength / deathDuration;
        float endTime = Time.time + deathDuration;
        Vector3 currentDissolveOffset = dissolveStartOffset;
        while (Time.time < endTime)
        {
            yield return null;
            currentDissolveOffset += changeSpeed * Time.deltaTime * dissolveChangeDirection;
            foreach (var material in materials)
            {
                material.SetDirectionDissolve(currentDissolveOffset);
            }
        }
    }

    /// <summary>
    /// Задать состояние объекта. 
    /// Используется для восстановления состояния объекта при возрождении на контрольной точке.
    /// Потенциально метод может быть использован для загрузки из файла,
    /// но сейчас мы используем его только для того, чтобы при начале игры с контрольной точки
    /// восстановить первоначальное состояние объекта
    /// </summary>
    /// <param name="state"></param>
    public override void SetState(object state)
    {
        base.SetState(state);
        foreach (var material in materials)
        {
            material.SetDirectionDissolve(dissolveStartOffset);
        }
    }

}
