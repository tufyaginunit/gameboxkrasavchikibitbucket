using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// �������� ��������-������. ���� ������ ���� ������ �� �����.
/// ��� ��������� �� ����� ����� ���������� � ����� ������ � ���� ��������� ���� � ������.
/// ��������-�����. ������������� ��������� ��� ��������� �������� ������ ������� GameManager
/// </summary>
public class EnemyManager : MonoBehaviour
{
    /// <summary>
    /// ������ ���� ������
    /// </summary>
    private readonly List<IDamageable> enemies = new List<IDamageable>();

    /// <summary>
    /// ������ ������, ����������� � ������ ������ � ������ �����
    /// </summary>
    private readonly List<IDamageable> attackingEnemies = new List<IDamageable>();

    public static EnemyManager Instance { get; private set; }

    public List<IDamageable> Enemies => enemies;

    public List<IDamageable> AttackingEnemies => attackingEnemies;

    /// <summary>
    /// �������� ������ ����� � ����� ������
    /// </summary>
    /// <param name="enemy"></param>
    public void AddEnemy(IDamageable enemy)
    {
        if (!enemies.Contains(enemy))
        {
            enemies.Add(enemy);
        }
    }

    /// <summary>
    /// ������� ������ ����� �� ������
    /// </summary>
    /// <param name="enemy"></param>
    public void RemoveEnemy(IDamageable enemy)
    {
        enemies.Remove(enemy);
    }

    /// <summary>
    /// �������� �����, ������������ � ������ �����
    /// </summary>
    /// <param name="enemy"></param>
    public void AddAttackingEnemy(IDamageable enemy)
    {
        attackingEnemies.Add(enemy);
    }

    /// <summary>
    /// ������� ����� �� ������ ������, ����������� � ������ �����
    /// </summary>
    /// <param name="enemy"></param>
    public void RemoveAttackingEnemy(IDamageable enemy)
    {
        attackingEnemies.Remove(enemy);
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }

}
