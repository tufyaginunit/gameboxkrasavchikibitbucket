using System.Collections;
using UnityEngine;

/// <summary>
/// ����������� ����� ������� ������ ������������� �������.
/// </summary>
public abstract class DeathEffect : MonoBehaviour, ISaveable
{
    [SerializeField]
    [Tooltip("������������ ������")]
    protected DamageableObject damageableObject;

    [SerializeField]
    [Tooltip("������������ ������� ������")]
    protected float deathDuration;

    [SerializeField]
    [Tooltip("�������� �� Y-���������� ������� " +
        "(��� �������� ���������� ��� ����� ��� � ����)")]
    protected float smoothYShift;

    /// <summary>
    /// ���������� ������� ������
    /// </summary>
    protected virtual void ProcessDeath()
    {
        if (smoothYShift != 0)
        {
            StartCoroutine(PerformSmoothShift(deathDuration));
        }

        // ��������� ������ ����� ��������� �����
        StartCoroutine(DisableAfterTime(deathDuration));
    }

    protected virtual void Awake()
    {
        damageableObject.RegisterKilledListener(ProcessDeath);
    }

    protected virtual void OnDestroy()
    {
        if (damageableObject != null)
        {
            damageableObject.UnregisterKilledListener(ProcessDeath);
        }
    }

    /// <summary>
    /// ��������, ����������� (�������� ���������) ������ ����� ��������� �����
    /// </summary>
    /// <returns></returns>
    private IEnumerator DisableAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        damageableObject.gameObject.SetActive(false);
    }

    /// <summary>
    /// ��������, ����������� ������� �������� �� Y-����������
    /// </summary>
    /// <returns></returns>
    private IEnumerator PerformSmoothShift(float time)
    {
        float coordChangeSpeed = smoothYShift / time;
        float endTime = Time.time + deathDuration;
        while (Time.time < endTime)
        {
            damageableObject.Transform.Translate(new Vector3(0, coordChangeSpeed * Time.deltaTime, 0));
            yield return null;
        }
    }

    /// <summary>
    /// ������ ��������� �������. 
    /// ������������ ��� �������������� ��������� ������� ��� ����������� �� ����������� �����.
    /// ������������ ����� ����� ���� ����������� ��� �������� �� �����,
    /// �� ������ �� ���������� ��� ������ ��� ����, ����� ��� ������ ���� � ����������� �����
    /// ������������ �������������� ��������� �������
    /// </summary>
    /// <param name="state"></param>
    public virtual void SetState(object state)
    {
        damageableObject.gameObject.SetActive(true);
    }

    /// <summary>
    /// �������� ��������� �������.
    /// ������������ ����� ����� ���� ����������� ��� ���������� ������� � ����,
    /// ������ ��� �� ���������, ������� ���������� ������ ������ � �������� ���������
    /// </summary>
    /// <returns></returns>
    public virtual object GetState()
    {
        return "";
    }
}
