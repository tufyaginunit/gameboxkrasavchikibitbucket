﻿using UnityEngine;

/// <summary>
/// StateMachine-состояние процесса поиска врагом игрока за случайным препятствием.
/// Состояние, в которое переходит враг, когда потерял игрока из виду.
/// Требует, чтобы на объекте присутствовал компонент AISearchingBehindWallsStrategy (для выполнения
/// определённого алгоритма поиска).
/// Все состояния должны находиться в дочерних объектах объекта-врага, 
/// т.к. им требуются определённые компоненты на нём
/// </summary>
[RequireComponent(typeof(AISearchingBehindObstaclesStrategy))]
public class SearchingBehindObstaclesState : FsmState
{
    /// <summary>
    /// Движущийся объект ИИ
    /// </summary>
    private AIMovingObject movingObject;

    /// <summary>
    /// Объект поля зрения ИИ
    /// </summary>   
    private FieldOfView fov;

    /// <summary>
    /// Стратегия поиска
    /// </summary>
    private AISearchingBehindObstaclesStrategy searchingStrategy;

    /// <summary>
    /// Метод, вызываемый при переходе в состояние
    /// </summary>
    public override void OnStateEnter()
    {
        movingObject.SetAttackSpeed();

        searchingStrategy.Initialize(movingObject, fov, GameManager.Instance.Player.transform);
        searchingStrategy.StartMoving();
    }

    /// <summary>
    /// Метод, вызываемый при выходе из состояния
    /// </summary>
    public override void OnStateLeave()
    {
        searchingStrategy.StopMoving();
    }

    /// <summary>
    /// Выполняется ли поиск. Возвращает true, если процесс поиска ещё не завершён,
    /// false - если все действия по поиску были завершены
    /// </summary>
    /// <returns></returns>
    public override bool IsPerforming()
    {
        return !searchingStrategy.IsStopped();
    }

    private void Awake()
    {
        movingObject = GetComponentInParent<AIMovingObject>();
        fov = GetComponentInParent<FieldOfView>();
        searchingStrategy = GetComponent<AISearchingBehindObstaclesStrategy>();
    }
}
