using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachine-��������� �������� �������� �����.
/// ���������, � ������� ��������� ����, ����� ��� ������� �������� ������ ����.
/// ��� ��������� ������ ���������� � �������� �������� �������-�����, 
/// �.�. �� ��������� ����������� ���������� �� ���
/// </summary>
public class DeathState : FsmState
{
    /// <summary>
    /// ���������� ������ ��
    /// </summary>
    private AIMovingObject movingObject;

    /// <summary>
    /// ���� ������ �� (��� ������ ����� ����� ��������� ����������� FOV �����)
    /// </summary>
    private FieldOfView fov;

    public override void OnStateEnter()
    {
        movingObject.SetEnabled(false);
        fov.SetEnabled(false);
    }

    private void Awake()
    {
        movingObject = GetComponentInParent<AIMovingObject>();
        fov = GetComponentInParent<FieldOfView>();
    }
}
