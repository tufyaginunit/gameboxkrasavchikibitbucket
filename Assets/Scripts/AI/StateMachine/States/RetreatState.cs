using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachine-��������� ������� �����, ���� �� �������� �������.
/// �������, ����� �� ������� ������������� ��������� AIRetreatStrategy (��� ��������
/// �� ������������ ��������� �������).
/// ��� ��������� ������ ���������� � �������� �������� �������-�����, 
/// �.�. �� ��������� ����������� ���������� �� ���
/// </summary>
[RequireComponent(typeof(AIRetreatStrategy))]
public class RetreatState : FsmState
{
    /// <summary>
    /// ��������� �������
    /// </summary>
    private AIRetreatStrategy retreatStrategy;

    /// <summary>
    /// ���������� ������ ��
    /// </summary>
    private AIMovingObject movingObject;

    /// <summary>
    /// ���� ������ ��
    /// </summary>
    private FieldOfView fov;

    /// <summary>
    /// �����, ���������� ��� �������� � ���������
    /// </summary>
    public override void OnStateEnter()
    {
        fov.SetAlertDetectionDelay();
        movingObject.SetIdleSpeed();

        retreatStrategy.Initialize(movingObject, fov, GameManager.Instance.Player, 
            GameManager.Instance.PlayerWeapon);
        retreatStrategy.StartMoving();
    }

    /// <summary>
    /// �����, ���������� ��� ������ �� ���������
    /// </summary>
    public override void OnStateLeave()
    {
        retreatStrategy.StopMoving();
    }

    private void Awake()
    {
        movingObject = GetComponentInParent<AIMovingObject>();
        fov = GetComponentInParent<FieldOfView>();

        retreatStrategy = GetComponent<AIRetreatStrategy>();
    }
}
