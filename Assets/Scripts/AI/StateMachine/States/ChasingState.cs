using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachine-��������� ������ ����� �� �������.
/// ���������, � ������� ��������� ����, ����� ������� ������ �� ����.
/// �������, ����� �� ������� ������������� ��������� AIChasingStrategy (��� ����������
/// ������������� ��������� ������).
/// ��� ��������� ������ ���������� � �������� �������� �������-�����, 
/// �.�. �� ��������� ����������� ���������� �� ���
/// </summary>
[RequireComponent(typeof(AIChasingStrategy))]
public class ChasingState : FsmState
{
    /// <summary>
    /// ���������� ������ ��
    /// </summary>
    private AIMovingObject movingObject;

    /// <summary>
    /// ��������� ������
    /// </summary>
    private AIChasingStrategy chasingStrategy;

    /// <summary>
    /// ���� ������ ��
    /// </summary>
    private FieldOfView fov;

    /// <summary>
    /// �����, ���������� ��� �������� � ���������
    /// </summary>
    public override void OnStateEnter()
    {
        fov.SetAlertDetectionDelay();
        movingObject.SetAttackSpeed();

        chasingStrategy.Initialize(movingObject, GameManager.Instance.Player.transform);
        chasingStrategy.StartMoving();
    }

    /// <summary>
    /// �����, ���������� ��� ������ �� ���������
    /// </summary>
    public override void OnStateLeave()
    {
        chasingStrategy.StopMoving();
    }

    private void Awake()
    {
        movingObject = GetComponentInParent<AIMovingObject>();
        fov = GetComponentInParent<FieldOfView>();

        chasingStrategy = GetComponent<AIChasingStrategy>();
    }
}
