using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachine-������� ��������� ���������� �������� � �����-���� ��������� 
/// (������ Has exit time � ���������)
/// </summary>
public class IsStateOver : FsmCondition
{
    /// <summary>
    /// ���������, ��������������� �� �������
    /// </summary>
    /// <param name="curr"></param>
    /// <param name="next"></param>
    /// <returns></returns>
    public override bool IsSatisfied(FsmState curr, FsmState next)
    {
        return !curr.IsPerforming();
    }
}
