using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachine-�������, �����������, ��������� �� ���� ��� ������ ������
/// </summary>
public class IsUnderAttack : FsmCondition
{
    [SerializeField]
    [Tooltip("�������� �� ������� ���������. " +
        "���� ��, �� �� �� ������ �� ���� ������� � �� �� ����� ���������" +
        "� ������� ������ ��������� ������ ��")]
    private bool isTrigger = false;

    /// <summary>
    /// ������ �������� ����� (����, ������� ������ ����� ��������� �����)
    /// </summary>
    private IDamageable healthObject;

    /// <summary>
    /// ���������� ������ ������
    /// </summary>
    private WeaponController attackerWeaponController;

    /// <summary>
    /// ���������, ��������������� �� �������
    /// </summary>
    /// <param name="curr"></param>
    /// <param name="next"></param>
    /// <returns></returns>
    public override bool IsSatisfied(FsmState curr, FsmState next)
    {
        if (isTrigger && curr == next)
        {
            return false;
        }

        // ���� ��������� ��� ������, ���� � ������ ������ ����� ������� ����������� 
        // ����� ����, � ���� ����� �������� ������ �������� ������� �����
        return attackerWeaponController.IsHitPerforming() && 
            attackerWeaponController.GetTarget() == healthObject;
    }

    private void Awake()
    {
        healthObject = GetComponentInParent<IDamageable>();
    }

    private void Start()
    {
        attackerWeaponController = GameManager.Instance.PlayerWeapon;
    }
}
