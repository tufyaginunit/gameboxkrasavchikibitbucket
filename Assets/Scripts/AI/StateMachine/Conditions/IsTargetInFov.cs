using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachine-������� ��������� ������ � ���� ��������� �����.
/// ����� ���������� true, ���� ���� �����, ��� ����� ������� ��� ��������
/// (�.�. �������, ��� ������� �������� ������� � ��� ������� �����)
/// </summary>
public class IsTargetInFov : FsmCondition
{    
    /// <summary>
    /// ���� ��������� �����
    /// </summary>
    private FieldOfView fov;

    /// <summary>
    /// ���������, ��������������� �� �������
    /// </summary>
    /// <param name="curr"></param>
    /// <param name="next"></param>
    /// <returns></returns>
    public override bool IsSatisfied(FsmState curr, FsmState next)
    {
        return fov.DetectingState != FieldOfView.TargetDetectingState.NotDetected;
    }

    private void Awake()
    {
        fov = GetComponentInParent<FieldOfView>();
    }

}
