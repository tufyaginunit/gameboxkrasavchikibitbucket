using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachine-������� ����������� ����� ������ ������ 
/// ���� �������� ���� ���������, ����� �� �� �� ������� ����� ������� �� ������ �� ������ ������
/// ������ ������� �������� �������������, �.�. ����������� �� ������ ����, � ����� ����������
/// �������, �������� � ���������� � �������� Period
/// </summary>
public class CanHitTarget : FsmCondPolled
{
    /// <summary>
    /// ����, � ������� �������� �������
    /// </summary>
    private IDamageable target;

    /// <summary>
    /// ���������� ������, �� �������� ��������
    /// </summary>
    private WeaponController weaponController;

    /// <summary>
    /// ���������, ����������� �� �������
    /// </summary>
    /// <returns></returns>
    protected override bool EvaluateCondition()
    {
        if (target == null || weaponController == null)
        {
            return false;
        }

        // ��������, ����� �� �� ������� ����� ������� �� ����,
        // �.�. ��������� �� ���� � ������� ����� � ��� �� ����������� �� ����
        return weaponController.CheckTargetAttackDistance(target) == AttackCheckResult.Ok
            && !weaponController.ThereAreObstaclesOnAttackWay(target);
    }

    private void Awake()
    {
        weaponController = GetComponentInParent<WeaponController>();
    }

    private void Start()
    {
        target = GameManager.Instance.Player;
    }
}
