﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Стратегия движения, при которой враг старается выбраться из препятствия,
/// которое стало непрозрачным.
/// Эта стратегия не относится к StateMachine, поскольку ситуация, когда необходимо выбраться из
/// препятствия может возникнуть в любой момент, и при этом необходимо не прерывать текущее действие,
/// которое выполняет объект ИИ, а ставить его на паузу до тех пор, пока враг не выберется из препятствия.
/// </summary>
public class GetOutOfObstacleStrategy: AIMovementStrategy
{
    /// <summary>
    /// Расстояние до точки снаружи препятствия, при котором считаем, что враг выбрался из препятствия
    /// </summary>    
    private const float MinDistanceToOutsidePoint = 0.5f;

    /// <summary>
    /// Квадрат расстояния (для оптимизации)
    /// </summary>
    private const float MinSqrDistanceToOutsidePoint = MinDistanceToOutsidePoint * MinDistanceToOutsidePoint;

    /// <summary>
    /// Список препятствий, на которых сейчас стоит объект ИИ
    /// </summary>
    private List<IObstacle> obstacles;

    /// <summary>
    /// Инициализировать объект стратегии движения
    /// (данный метод необходимо вызывать перед вызовом StartMoving)
    /// </summary>
    /// <param name="movingObject">Движущийся объект ИИ</param>
    /// <param name="obstacles">Список препятствий, на которых сейчас стоит объект ИИ</param>
    public void Initialize(AIMovingObject movingObject, List<IObstacle> obstacles)
    {
        BaseInitialize(movingObject);
        this.obstacles = obstacles;
    }

    /// <summary>
    /// Начать движение в соответствии со стратегией
    /// </summary>
    public override void StartMoving()
    {
        movingObject.SetEnabled(false);
        base.StartMoving();
    }

    /// <summary>
    /// Остановить движение
    /// </summary>
    public override void StopMoving()
    {
        base.StopMoving();
        if (movingObject != null)
        {
            movingObject.SetEnabled(true);
        }       
    }

    /// <summary>
    /// Корутина, которая выполняет движение к ближайшей точке за пределами препятствия
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator PerformMoving()
    {
        List<ObstacleHidingPoint> obstaclesPoints = ObstacleManager.Instance.GetObstaclesHidingPoints(obstacles);

        ObstacleHidingPoint pointToMove = null;
        float minSqrDist = float.MaxValue;
        foreach (var point in obstaclesPoints)
        {
            float dist = movingObject.GetSqrDistanceToPoint(point.Point);
            if (dist < minSqrDist)
            {
                pointToMove = point;
                minSqrDist = dist;
            }
        }

        if (pointToMove == null)
        {
            StopMoving();
            yield break;
        }

        Vector3 destination = pointToMove.Point;
        
        while (movingObject.GetSqrDistanceToPoint(pointToMove.Point) > MinSqrDistanceToOutsidePoint)
        {
            yield return new WaitForFixedUpdate();
            destination.y = movingObject.Position.y;
            movingObject.transform.position = Vector3.Lerp(movingObject.transform.position, pointToMove.Point,
                Time.fixedDeltaTime * movingObject.MaxSpeed);
        }

        StopMoving();
    }
}
