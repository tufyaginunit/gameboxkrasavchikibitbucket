using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ��������� ��������, ����������� ��� ��������� �� �� ����
/// </summary>
public class AIAimingStrategy : AIMovementStrategy
{
    /// <summary>
    /// ���� ��� ���������
    /// </summary>
    private IDamageable target;

    /// <summary>
    /// ���������������� ������ ��������� ��������
    /// (������ ����� ���������� �������� ����� ������� StartMoving)
    /// </summary>
    /// <param name="movingObject">���������� ������ ��</param>
    /// <param name="target">���� ���������</param>
    public void Initialize(AIMovingObject movingObject, IDamageable target)
    {
        BaseInitialize(movingObject);
        this.target = target;
    }

    /// <summary>
    /// �������� �������� ��������, ����������� ��������� �� ����
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator PerformMoving()
    {
        if (target == null)
        {
            movingCoroutine = null;
            yield break;
        }

        movingObject.SetEnabledAutomaticRotation(false);
        StartCoroutine(PerformInfiniteLookAt(target.Transform, movingObject.AttackRotationSpeed));
        while (target != null && target.IsAlive())
        {
            yield return null;
        }

        movingCoroutine = null;
    }
}
