﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Абстрактный класс стратегии движения ИИ.
/// От него наследуются классы, которые представляют определённые алгоритмы движения врагов
/// в различных ситуациях.
/// Как правило, добавляются на тот же объект, который представляет соответствующее 
/// состояние StateMachine
/// </summary>
public abstract class AIMovementStrategy : MonoBehaviour
{
    /// <summary>
    /// Максимальная разница между текущим углом и целевым, 
    /// при которой считаем, что объект ИИ повёрнут к цели
    /// </summary>
    const float LookAtAngleDifferenceEpsilon = 0.1f;

    /// <summary>
    /// Движущийся объект, находящегося под управлением ИИ
    /// </summary>
    protected AIMovingObject movingObject;

    /// <summary>
    /// Корутина стратегии движения
    /// </summary>
    protected Coroutine movingCoroutine = null;

    /// <summary>
    /// Инициализатор базового класса
    /// </summary>
    /// <param name="movingObject">Движущийся объект, находящегося под управлением ИИ</param>
    public void BaseInitialize(AIMovingObject movingObject)
    {
        this.movingObject = movingObject;
    }

    /// <summary>
    /// Проверить, выполняется ли движение
    /// </summary>
    /// <returns></returns>
    public bool IsStopped()
    {
        return movingCoroutine == null;
    }

    /// <summary>
    /// Начать движение в соответствии со стратегией
    /// </summary>
    public virtual void StartMoving()
    {
        movingObject.Stop();
        if (movingCoroutine == null)
        {
            movingCoroutine = StartCoroutine(PerformMoving());
        }
    }

    /// <summary>
    /// Остановить движение
    /// </summary>
    public virtual void StopMoving()
    {
        StopAllCoroutines();
        movingCoroutine = null;
        if (movingObject != null)
        {
            movingObject.Stop();
        }

    }

    /// <summary>
    /// Корутина процесса движения (конкретное поведение определяется в наследниках)
    /// </summary>
    /// <returns></returns>
    protected abstract IEnumerator PerformMoving();

    /// <summary>
    /// Корутина бесконечного плавного выполнения поворота влево-вправо
    /// </summary>
    /// <param name="fullAngle">Полный угол поворота (от крайнего левого положения к крайнему правому)</param>
    /// <param name="rotationSpeed">Скорость поворота</param>
    /// <returns></returns>
    protected IEnumerator PerformInfiniteRotatingLeftRight(float fullAngle, float rotationSpeed)
    {
        // Вначале берем только половину максимального угла, т.к. персонаж смотрит прямо,
        // и нам нужно будет повернуться только наполовину
        // При повороте в обратную сторону будетм брать уже целый угол, т.к. именно на него
        // нужно будет повернуться, чтобы посмотреть в противоположную сторону
        int direction = fullAngle > 0 ? 1 : -1;
        fullAngle = Mathf.Abs(fullAngle);
        float maxRotationAngle = fullAngle * .5f;
        float fullRotation = 0;
        while (true)
        {
            yield return new WaitForFixedUpdate();

            float rotationDelta = rotationSpeed * Time.fixedDeltaTime;
            movingObject.Rotate(direction * rotationDelta);

            fullRotation += rotationDelta;
            if (fullRotation >= maxRotationAngle)
            {
                fullRotation = 0;
                direction = -direction;
                maxRotationAngle = fullAngle;
            }
        }
    }

    protected IEnumerator PerformRotatingLeftRight(float fullAngle, float rotationSpeed, float duration,
        CoroutineTask coroutineTask)
    {
        // Выполняем повороты влево-вправо отведённое для этого время
        Coroutine rotatingCoroutine = StartCoroutine(
            PerformInfiniteRotatingLeftRight(fullAngle, rotationSpeed));

        float endTime = Time.time + duration;
        while (Time.time < endTime)
        {
            yield return new WaitForFixedUpdate();
        }
        this.StopAndNullCoroutine(ref rotatingCoroutine);
        coroutineTask.Stop();
    }

    /// <summary>
    /// Корутина выполнения плавного поворота на определённый угол
    /// </summary>
    /// <param name="angle">Угол, на который нужно повернуться. 
    /// При положительном значении происходит поворот вправо, при отрицательном - влево</param>
    /// <param name="rotationSpeed">Скорость поворота</param>
    /// <returns></returns>
    protected IEnumerator PerformRotating(float angle, float rotationSpeed)
    {
        int direction = angle > 0 ? 1 : -1;
        float absAngle = Mathf.Abs(angle);
        float currentAngle = 0;
        while (currentAngle < absAngle)
        {
            yield return new WaitForFixedUpdate();

            float rotationDelta = rotationSpeed * Time.fixedDeltaTime;
            movingObject.Rotate(rotationDelta * direction);
            currentAngle += rotationDelta;
        }
    }

    /// <summary>
    /// Корутина выполнения плавного поворота на определённый угол
    /// </summary>
    /// <param name="angle">Угол, на который нужно повернуться. 
    /// При положительном значении происходит поворот вправо, при отрицательном - влево</param>
    /// <param name="rotationSpeed">Скорость поворота</param>
    /// <returns></returns>
    protected IEnumerator PerformRotating(float angle, float rotationSpeed, CoroutineTask coroutineTask)
    {
        float fullRotationTime = Mathf.Abs(angle) / rotationSpeed;

        Coroutine rotatingCoroutine = StartCoroutine(PerformRotating(angle, rotationSpeed));
        
        float endTime = Time.time + fullRotationTime;
        while (Time.time < endTime)
        {
            if (coroutineTask.IsCancellationRequested)
            {
                break;
            }
            yield return new WaitForFixedUpdate();
        }

        this.StopAndNullCoroutine(ref rotatingCoroutine);

        coroutineTask.Stop();
    }

    /// <summary>
    /// Корутина выполнения плавного бесконечного поворота к цели
    /// </summary>
    /// <param name="target">Цель, к которой нужно постоянно быть повёрнутым</param>
    /// <param name="rotationSpeed">Скорость поворота</param>
    /// <returns></returns>
    protected IEnumerator PerformInfiniteLookAt(Transform target, float rotationSpeed)
    {      
        while (target != null)
        {
            yield return new WaitForFixedUpdate();

            PerformOneTickRotation(target.position, rotationSpeed);
        }
    }

    /// <summary>
    /// Корутина выполнения плавного бесконечного поворота к точке
    /// </summary>
    /// <param name="position">Точка, к которой нужно постоянно быть повёрнутым</param>
    /// <param name="rotationSpeed">Скорость поворота</param>
    /// <returns></returns>
    protected IEnumerator PerformInfiniteLookAt(Vector3 position, float rotationSpeed)
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();

            PerformOneTickRotation(position, rotationSpeed);
        }
    }

    /// <summary>
    /// Получить угол, на который нужно повернуться, чтобы смотреть на целевую точку
    /// Положительное значение означает, что нужно осуществить поворот вправо, отрицательное - влево
    /// </summary>
    /// <param name="targetPosition">Целевая точка</param>
    /// <returns></returns>
    private float GetLookAtAngle(Vector3 targetPosition)
    {
        return VectorFunc.GetSignedAngleXZ(movingObject.transform.forward,
            targetPosition - movingObject.Position);
    }

    /// <summary>
    /// Выполнить часть поворота к целевой точке, который произойдет за один кадр
    /// </summary>
    /// <param name="targetPosition">Целевая точка</param>
    /// <param name="rotationSpeed">Скорость поворота</param>
    private void PerformOneTickRotation(Vector3 targetPosition, float rotationSpeed)
    {
        float angle = GetLookAtAngle(targetPosition);
        float absAngle = Mathf.Abs(angle);
        if (absAngle < LookAtAngleDifferenceEpsilon)
        {
            return;
        }

        // Если величина поворота за один тик превышает величину необходимого доворота цели,
        // то мгновенно доворачиваемся к цели, чтобы избежать подёргиваний поля зрения
        float rotationDelta = rotationSpeed * Time.fixedDeltaTime;
        if (Mathf.Abs(angle) < rotationDelta)
        {
            movingObject.Rotate(targetPosition);
            return;
        }

        int direction = angle > 0 ? 1 : -1;
        movingObject.Rotate(direction * rotationDelta);
    }

}
