using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ����������� ����� ��������� ����� ��.
/// �� ���� ����������� ������, ������� ������������ ����������� ��������� ����� ������
/// � ��������� ���������.
/// ��� �������, ����������� �� ��� �� ������, ������� ������������ ��������������� 
/// ��������� StateMachine
/// </summary>
public abstract class AIAttackingStrategy : MonoBehaviour
{
    /// <summary>
    /// ���������� ������
    /// </summary>
    protected WeaponController weaponController;
    
    /// <summary>
    /// ����, ������� ����� ���������
    /// </summary>
    protected IDamageable target;
    
    /// <summary>
    /// �������� �������� �����
    /// </summary>
    protected Coroutine attackingCoroutine = null;

    /// <summary>
    /// ���������������� ��������� �����
    /// </summary>
    /// <param name="weaponController">���������� ������, �� �������� ������ �����</param>
    /// <param name="target">����, ������� ����� ���������</param>
    public virtual void Initialize(WeaponController weaponController, IDamageable target)
    {
        this.weaponController = weaponController;
        this.target = target;
    }

    /// <summary>
    /// ������ �����
    /// </summary>
    public virtual void StartAttacking()
    {
        if (attackingCoroutine == null)
        {
            attackingCoroutine = StartCoroutine(PerformAttacking());
        }
    }

    /// <summary>
    /// ���������� �����
    /// </summary>
    public virtual void StopAttacking()
    {
        if (attackingCoroutine != null)
        {
            StopAllCoroutines();
            attackingCoroutine = null;

            weaponController.InterruptAttack();
        }
    }

    /// <summary>
    /// �������� �������� ����� (���������� ��������� ������������ � �����������)
    /// </summary>
    /// <returns></returns>
    protected abstract IEnumerator PerformAttacking();
}
