using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// ��������� ������� �����, ����� ��� ������� �����.
/// </summary>
public class AIRetreatStrategy : AIMovementStrategy
{
    [SerializeField]
    [Tooltip("������������� � ��������, � ������� ���� ����� ��������� " +
        "���� ������� ������, � ������� �������")]
    private float refreshPointDelay = 1f;
    
    [SerializeField]
    [Tooltip("�����, ����� ������� ����� ����� ������ ��������� " +
        "���������� ����������� �� ������� ����, ����� �� �� ���� ����������")]
    private float retryChangingObstaclesDelay = 5f;

    /// <summary>
    /// ���� ������ �����
    /// </summary>
    private FieldOfView fov = null;

    /// <summary>
    /// ������, ������� ������� ����� (�.�. �����)
    /// </summary>
    private IDamageable attacker = null;

    /// <summary>
    /// ���������� ������ ������
    /// </summary>
    private WeaponController attackerWeaponController = null;

    /// <summary>
    /// ��������� ����� �����������, ����������� � �������� ���� ������ �����
    /// </summary>
    private readonly List<ObstacleHidingPoint> pointsInideFov = new List<ObstacleHidingPoint>();

    /// <summary>
    /// ��������� ����� �����������, ����������� �� ��������� ���� ������ �����
    /// </summary>    
    private readonly List<ObstacleHidingPoint> pointsOutsideFov = new List<ObstacleHidingPoint>();

    /// <summary>
    /// ����������� �������������� �����, �� �������� ����� ����� ���������� ����
    /// (��������� ��� ���� ������ ��� �������)
    /// </summary>
    private List<ObstacleHidingPoint> currentAdditionalHidingPoints = new List<ObstacleHidingPoint>();

    /// <summary>
    /// ������� ��������� ����������� �����, �� ������� ����� ���������� 
    /// (��������� ��� ���� ������ ��� �������)
    /// </summary>
    private ObstacleHidingPoint currentHidingPoint = null;

    /// <summary>
    /// ��������� �������� ���� ����������� � ����������� �����
    /// (��������� ��� ���� ������ ��� �������)
    /// </summary>    
    private List<HidingPointPath> currentRetreatPathList = null;

    /// <summary>
    /// ���������������� ������ ��������� ��������
    /// (������ ����� ���������� �������� ����� ������� StartMoving)
    /// </summary>
    /// <param name="movingObject">���������� ������ ��</param>
    /// <param name="fov">���� ������ �����</param>
    /// <param name="attacker">������, ������� ������� ����� (�.�. �����)</param>
    /// <param name="attackerWeaponController">���������� ������ ������</param>
    public void Initialize(AIMovingObject movingObject, FieldOfView fov, IDamageable attacker,
        WeaponController attackerWeaponController)
    {
        BaseInitialize(movingObject);
        this.attacker = attacker;
        this.fov = fov;
        this.attackerWeaponController = attackerWeaponController;
    }

    /// <summary>
    /// ��������, ������� ��������� ����� ������������� ����� �������� �����������,
    /// � ����������� �� ���������� �������, � �������� �� ���������� ��������
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator PerformMoving()
    {
        // ��������� �������������� ������� �� ������� ����, �.�. ���� ����� �� ����� �������
        // � ����������. �������� ��������, ����������� ���������� ������������� � ����������
        movingObject.SetEnabledAutomaticRotation(false);
        Coroutine rotatingCoroutine = StartCoroutine(
            PerformInfiniteLookAt(attacker.Transform, movingObject.AttackRotationSpeed));

        ObstacleHidingPoint hidingPoint = null;

        // �� ��������� ������� ��������� ����� ������ ����� ����� � �������������, ���������
        // ���� ������������.
        bool canTryHideBehindChangingObstacle = true;
        float nextTryToCheckChangingObstacles = 0;

        while (attacker != null && attacker.IsAlive())
        {
            // �������� ���������� ��������� �����, �� ������� �������� ���������� ����.
            // ���� �� ������� ���������� �� �����������, �.�. ��� ����� ����������,
            // �� ����� ��������� ������������ ����������� ��������� �����
            if (hidingPoint != null && hidingPoint.Obstacle.Type == ObstacleType.OpacityChanging
                && !hidingPoint.Obstacle.IsOpaque())
            {
                canTryHideBehindChangingObstacle = false;
                nextTryToCheckChangingObstacles = Time.time + retryChangingObstaclesDelay;
            }

            // ������� ����������� ����� �� �����, �� ������� ����� ���������� � �������� � ���
            if (FindOptimalHidingPoint(canTryHideBehindChangingObstacle, out hidingPoint))
            {
                movingObject.MoveToPoint(hidingPoint.Point);
                currentHidingPoint = hidingPoint;
            }

            yield return new WaitForSeconds(refreshPointDelay);

            // �������� �������� ������������ �����������, ���� ������� ������
            if (!canTryHideBehindChangingObstacle &&
                Time.time > nextTryToCheckChangingObstacles)
            {
                canTryHideBehindChangingObstacle = true;
            }
        }

        this.StopAndNullCoroutine(ref rotatingCoroutine);
        movingCoroutine = null;
    }

    /// <summary>
    /// ��������� ����������� ��������� �����, �� �������� ����� ���������� ����
    /// ����� ��������� ������ ����� ������ � ������� ������� ������ ����� (pointsInideFov, pointsOutsideFov),
    /// � ��������� �������������� ����� �������� ������������ �����
    /// </summary>
    /// <param name="canHideBehindChangingObstacle">��������� �� ��������� �� ������������� �������������</param>
    private void FillPointGroups(bool canHideBehindChangingObstacle)
    {
        pointsInideFov.Clear();
        pointsOutsideFov.Clear();

        // �������� ��� ����� ������ ����������� �� ��������� �����������
        List<ObstacleHidingPoint> hidingPoints = ObstacleManager.Instance.HidingPoints.ToList();

        // ��������� �����, ������� ��������� ������ ���������� �����������
        for (int i = hidingPoints.Count - 1; i >= 0; i--)
        {
            if (!hidingPoints[i].Obstacle.IsOpaque())
            {
                hidingPoints.RemoveAt(i);
            }
        }

        // ��������� �����, ������������� ������������ ������������, ���� �� ���� ������ ������ ���������
        if (!canHideBehindChangingObstacle)
        {
            for (int i = hidingPoints.Count - 1; i >= 0; i--)
            {
                if (hidingPoints[i].Obstacle.Type == ObstacleType.OpacityChanging)
                {
                    hidingPoints.RemoveAt(i);
                }
            }
        }

        // ���������� ����� ����������� �� ��� ������: ������ ������� ������ � �������
        foreach (var point in hidingPoints)
        {
            if (fov.IsPointInFovRadius(point.Point))
            {
                pointsInideFov.Add(point);
            }
            else
            {
                pointsOutsideFov.Add(point);
            }
        }

        // ���� ����������� ����� �����, ����� ��������� ���, ��� �� �� ����� �� �������
        // �������������� ������ ������ ���� ������ ���������� �� �����.
        // ������� ��������� ������������ ����� �� �������������, ������� �������������� � �����������
        // �� ������� ����� ����� ������, � �� �������� ����� ����� ����� ����������.
        // ��� ����������� ������������ ����� ����� ������ ��� ��� �����������, ������� ��������� � ��������
        // ���� ������. 
        
        List<IObstacle> obstaclesInsideFov = ObstacleManager.Instance.GetHidingPointsObstacles(pointsInideFov);
        
        currentAdditionalHidingPoints = ObstacleManager.Instance.GetPointsToHideFromPointOfView(
            attackerWeaponController.AttackPoint.position, obstaclesInsideFov);
        
        pointsInideFov.AddRange(currentAdditionalHidingPoints);
    }

    /// <summary>
    /// ����� ����������� ����� ��� ������� ����� �� �����
    /// </summary>
    /// <param name="canHideBehindChangingObstacle">��������� �� ��������� �� ������������� �������������</param>
    /// <param name="optimalPoint">��������� �����</param>
    /// <returns>true - ����� �������, false - ����� �� �������</returns>
    private bool FindOptimalHidingPoint(bool canHideBehindChangingObstacle, out ObstacleHidingPoint optimalPoint)
    {
        optimalPoint = null;
        
        // ��� ����������� ��������� �� ���������� ���������� ���� �� ����� �������
        if (!movingObject.IsAINavigationEnabled())
        {
            return false;
        }

        // ���������� �������������� ��������� ����� ��� �������
        FillPointGroups(canHideBehindChangingObstacle);

        List<HidingPointPath> retreatPathList = new List<HidingPointPath>();
        currentRetreatPathList = retreatPathList;
        
        // �������� ����� ���������� ����� � ������� ������
        FillRetreatPathList(pointsInideFov, retreatPathList);
        if (FindOptimalPointByPathList(retreatPathList, out optimalPoint))
        {
            return true;
        }

        // ���� �� ���������� ����� �����, ���� �� ��������� ������� ������
        FillRetreatPathList(pointsOutsideFov, retreatPathList);
        if (FindOptimalPointByPathList(retreatPathList, out optimalPoint))
        {
            currentRetreatPathList = retreatPathList;
            return true;
        }

        return false;
    }

    /// <summary>
    /// ��������� ������ ���������� ����� ������ � ��������� ������, �� �������� ����� ����������
    /// ����� ����� ���������� �������� �� ����������� ���������� ����� � ����� �����, �� ��������
    /// ������ �������� �� �����
    /// </summary>
    /// <param name="hidingPoints">������ ��������� ����� ������</param>
    /// <param name="retreatPathList">�������������� ������ ���������� ����� ������ � ���</param>
    private void FillRetreatPathList(List<ObstacleHidingPoint> hidingPoints, List<HidingPointPath> retreatPathList)
    {
        retreatPathList.Clear();
        foreach (var point in hidingPoints)
        {
            if (CanHideOnPoint(point.Point) &&
                movingObject.CalculatePathToPoint(point.Point, out Vector3[] pathPoints))
            {
                retreatPathList.Add(new HidingPointPath(point, VectorFunc.GetPathLength(pathPoints)));
            }
        }
    }

    /// <summary>
    /// ����� ����������� ����� ������� �� ����������� ������ �����, 
    /// ��������� �� ������������ ���������� ��������� ���� � ���
    /// </summary>
    /// <param name="retreatPathList">������ ���������� ����� � ������</param>
    /// <param name="point">��������� ����������� �����</param>
    /// <returns></returns>
    private bool FindOptimalPointByPathList(List<HidingPointPath> retreatPathList, out ObstacleHidingPoint point)
    {
        // ��������: ������������� ������ ����� �� ������� ���������� �� �� ����� � ������.
        // ��� ������ ����� �������������� � ��� (������������������).
        // ����� �������� ����� ����������������, ���� ����� �� �� ���� �����, ��� ������.
        // ������������ ������� ��������� ���, ����� �������� ���������� �� ����� �� ����� ���� ������� 
        // ������� �� ������������������ (����� ����, ���� ����� � ��� ��������� �������, �� �������
        // �� ����� ������� �������� �� ������)

        point = null;
        float currentPointWeight = -float.MaxValue;
        foreach (var path in retreatPathList)
        {
            float attackerDistanceToPoint = VectorFunc.GetDistanceXZ(path.point.Point, attacker.Transform.position);
            float weight = (attackerDistanceToPoint - path.pathLength) / path.pathLength;
            if (weight > currentPointWeight)
            {
                currentPointWeight = weight;
                point = path.point;
            }
        }

        return point != null;
    }

    /// <summary>
    /// ���������, ����� �� ���������� �� ������ �� �����
    /// </summary>
    /// <param name="point">����������� �����</param>
    /// <returns></returns>
    private bool CanHideOnPoint(Vector3 point)
    {
        // ���� ���� ����������� �� ����� ����� ������, ������ ��� ����� ��������,
        // ����� �� �� ����������.

        Vector3 attackPoint = attackerWeaponController.AttackPoint.position;
        Vector3 vectorToPoint = point - attackPoint;
        vectorToPoint.y = 0;
        float distance = vectorToPoint.magnitude;

        return AreObstaclesOnWay(attackPoint, vectorToPoint, distance); 
    }

    /// <summary>
    /// ���������, ���� �� ����������� �� �������
    /// </summary>
    /// <param name="startPoint">��������� �����</param>
    /// <param name="direciton">����������� �������</param>
    /// <param name="distance">����� �������</param>
    /// <returns></returns>
    private bool AreObstaclesOnWay(Vector3 startPoint, Vector3 direciton, float distance)
    {
        return Physics.Raycast(startPoint, direciton, distance,
            GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore);
    }

    /// <summary>
    /// ���������� ��������� �� ������ �����
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        if (ObstacleManager.Instance == null || !ObstacleManager.Instance.DrawHidingPoints)
        {
            return;
        }

        foreach (var item in currentAdditionalHidingPoints)
        {
            Gizmos.color = Color.gray;
            Gizmos.DrawCube(item.Point, new Vector3(0.2f, 0.2f, 0.2f));
        }

        // ����������� ������� ������� �����, ��������� ��� ����, ����� ����������
        if (currentHidingPoint != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawCube(currentHidingPoint.Point, new Vector3(1, 1, 1));
        }

        // ������������ ������ ��������� �����, �� ������� ���� �������� �����������
        if (currentRetreatPathList != null)
        {
            foreach (var item in currentRetreatPathList)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawCube(item.point.Point, new Vector3(0.4f, 0.4f, 0.4f));
            }
        }
    }

}
