﻿/// <summary>
/// Константы событий получения ввода от игрока, 
/// используемые при получении значений событий класса Input
/// </summary>
public static class ControlConstants
{
    public const string HorizontalMove = "Horizontal";
    public const string VerticalMove = "Vertical";
    public const string AttackButton = "Fire1";
}
