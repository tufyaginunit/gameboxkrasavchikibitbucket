using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ����� ����� �������� � ���������, ������� ����� ����������� � ����� ������ ������
/// </summary>
public static class VectorFunc
{
    /// <summary>
    /// ������������ ��������� ��������
    /// </summary>
    public const float MaxRaycastDistance = 1000;

    /// <summary>
    /// �������� ���������� ����� ������� � ��������� XZ
    /// </summary>
    /// <param name="point1"></param>
    /// <param name="point2"></param>
    /// <returns></returns>
    public static float GetDistanceXZ(Vector3 point1, Vector3 point2)
    {
        point1.y = 0;
        point2.y = 0;
        return Vector3.Distance(point1, point2);
    }

    /// <summary>
    /// �������� ������� ���������� ����� ������� � ��������� XZ
    /// </summary>
    /// <param name="point1"></param>
    /// <param name="point2"></param>
    /// <returns></returns>
    public static float GetSqrDistanceXZ(Vector3 point1, Vector3 point2)
    {
        point1.y = 0;
        point2.y = 0;
        return Vector3.SqrMagnitude(point2 - point1);
    }

    /// <summary>
    /// �������� ��������� ������, ��������� �� �������� ���� ������������ Vector3.forward
    /// </summary>
    /// <param name="angleInDegrees">���� ��������</param>
    /// <returns></returns>
    public static Vector3 GetRotatedUnitVector(float angleInDegrees)
    {
        float vectorAngleInRad = angleInDegrees * Mathf.Deg2Rad;
        return new Vector3(Mathf.Sin(vectorAngleInRad), 0, Mathf.Cos(vectorAngleInRad));
    }

    /// <summary>
    /// ��������� ������ �� ��������� ���� (�������������� ������ ����� ���������).
    /// </summary>
    /// <param name="vector">������, ������� ����� ���������</param>
    /// <param name="rotationInDegrees">���� ��������</param>
    /// <returns></returns>
    public static Vector3 GetRotatedUnitVector(Vector3 vector, float rotationInDegrees)
    {
        float globalAngle = GetSignedAngleXZ(Vector3.forward, vector);
        return GetRotatedUnitVector(globalAngle + rotationInDegrees);
    }

    /// <summary>
    /// ����� ��������� ��������, ������������� ���������� ����� ��� �������
    /// </summary>
    /// <param name="pathPoints">����� ��������</param>
    /// <returns></returns>
    public static float GetPathLength(Vector3[] pathPoints)
    {
        float pathLength = 0;
        if (pathPoints == null)
        {
            return pathLength;
        }

        for (int i = 1; i < pathPoints.Length; i++)
        {
            pathLength += Vector3.Distance(pathPoints[i - 1], pathPoints[i]);
        }

        return pathLength;
    }

    /// <summary>
    /// �������� �������� �������, ���������� �� ���� ���������, �� ��������� �����������
    /// </summary>
    /// <param name="point">����� ����� �������</param>
    /// <param name="direction">��������� ������, ��������� �� ���� ���������, �� ������� ����� ����� ��������</param>
    /// <returns></returns>
    public static Vector3 GetProjectionOnDirection(Vector3 point, Vector3 direction)
    {
        return direction * Vector3.Dot(point, direction);
    }

    /// <summary>
    /// �������� ���������� ���� �������� �� ������� 1 � ������� 2 � ��������� XZ � ������ ����������� ��������.
    /// ������������� ��������� ��������, ��� ������� ����� ������ �� ������� �������, ������������� - ������.
    /// </summary>
    /// <param name="vector1"></param>
    /// <param name="vector2"></param>
    /// <returns></returns>
    public static float GetSignedAngleXZ(Vector3 vector1, Vector3 vector2)
    {
        vector1.y = 0;
        vector2.y = 0;
        return Vector3.SignedAngle(vector1, vector2, Vector3.up);
    }
}
