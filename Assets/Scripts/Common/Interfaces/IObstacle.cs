using UnityEngine;

/// <summary>
/// ��������� �������-�����������
/// </summary>
public interface IObstacle
{
    /// <summary>
    /// ��������� �����������
    /// </summary>
    Transform Transform { get; }

    /// <summary>
    /// ��� �����������
    /// </summary>
    ObstacleType Type { get; }

    /// <summary>
    /// ����� �� � ������ ������ ������ ����� �����������
    /// </summary>
    /// <returns></returns>
    bool IsPassable();

    /// <summary>
    /// �������� � ������ ������ ����������� ��������� �������
    /// </summary>
    /// <returns></returns>
    bool IsOpaque();

    /// <summary>
    /// �������� ���������� �����������
    /// </summary>
    /// <returns></returns>
    Collider[] GetColliders();
}
