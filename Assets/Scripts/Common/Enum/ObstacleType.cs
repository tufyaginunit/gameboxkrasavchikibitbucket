﻿/// <summary>
/// Тип препятствия
/// </summary>
public enum ObstacleType
{
    /// <summary>
    /// Препятствие, меняющее свою прозрачность
    /// </summary>
    OpacityChanging,
    /// <summary>
    /// Всегда непрозрачное препятствие
    /// </summary>
    AlwaysOpaque
}
