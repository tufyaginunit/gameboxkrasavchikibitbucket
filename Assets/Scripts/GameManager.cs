using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ��������-����� ��������� ����. 
/// ������������� ������ � ��������� ������� ��������, � ������� ����� ���������� 
/// �� ������ ��������.
/// ����� �� �������� ���������� ��������� ����.
/// </summary>
public class GameManager : MonoBehaviour
{
    [SerializeField]
    [Tooltip("������� ���������� ����")]
    private GameController gameController = null;

    [SerializeField]
    [Tooltip("��� ������� ����� ����")]
    private string gameSceneName = null;

    [SerializeField]
    [Tooltip("������ HP ������")]
    private DamageableObject playerHealthObject = null;

    [SerializeField]
    [Tooltip("���������� ������ ������")]
    private WeaponController playerWeaponController = null;

    [SerializeField]
    [Tooltip("����, �� ������� ����������� ��� ��������� �����������")]
    private LayerMask obstacleLayers;

    [SerializeField]
    [Tooltip("����, �� ������� ����������� �����������, ���������� ���� ���������")]
    private LayerMask changingObstacleLayers;

    [SerializeField]
    [Tooltip("����, �� ������� ����������� �����")]
    private LayerMask enemyLayers;

    [SerializeField]
    [Tooltip("������� �����")]
    private float groundY = 0;

    [Tooltip("������ ����� ����� ������ � ������ ��� ������� �����")]
    [SerializeField]
    private float attackPointHeightFromGround = 1;

    public static GameManager Instance { get; private set; }

    public GameController Controller => gameController;
    public string GameSceneName => gameSceneName;
    public DamageableObject Player => playerHealthObject;
    public WeaponController PlayerWeapon => playerWeaponController;
    public LayerMask ObstacleLayers => obstacleLayers;
    public LayerMask ChangingObstacleLayers => changingObstacleLayers;
    public LayerMask EnemyLayers => enemyLayers;
    public float GroundY => groundY;

    // ���������� Y-���������� ����� ����� ������ � ������
    public float AttackPointY => groundY + attackPointHeightFromGround;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        } 
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }

}
